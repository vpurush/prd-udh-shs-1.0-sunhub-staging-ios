namespace SunshareDatabaseD2DLiveCopy
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LeadSourceSubCategory")]
    public partial class LeadSourceSubCategory
    {
        public int LeadSourceSubCategoryId { get; set; }

        public int LeadSourceId { get; set; }

        [StringLength(150)]
        public string LeadSourceSubCategoryName { get; set; }

        public bool IsActive { get; set; }

        public DateTime? DateCreated { get; set; }

        [StringLength(100)]
        public string UserCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [StringLength(100)]
        public string UserModified { get; set; }

        public virtual LeadSource LeadSource { get; set; }
    }
}
