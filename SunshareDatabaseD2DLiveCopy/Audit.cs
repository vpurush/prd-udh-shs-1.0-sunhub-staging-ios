namespace SunshareDatabaseD2DLiveCopy
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Audit")]
    public partial class Audit
    {
        public int AuditId { get; set; }

        public DateTime RivisionStamp { get; set; }

        [StringLength(100)]
        public string TableName { get; set; }

        public int Actions { get; set; }

        [Column(TypeName = "xml")]
        public string OldData { get; set; }

        [Column(TypeName = "xml")]
        public string NewData { get; set; }

        [StringLength(400)]
        public string ChangedColumns { get; set; }

        public DateTime? DateCreated { get; set; }

        [StringLength(100)]
        public string UserCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [StringLength(100)]
        public string UserModified { get; set; }
    }
}
