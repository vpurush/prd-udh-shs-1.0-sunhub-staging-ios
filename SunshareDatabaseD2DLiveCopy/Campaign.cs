namespace SunshareDatabaseD2DLiveCopy
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Campaign")]
    public partial class Campaign
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Campaign()
        {
            Lead = new HashSet<Lead>();
            LeadSource = new HashSet<LeadSource>();
        }

        public int CampaignId { get; set; }

        [StringLength(200)]
        public string CampaignName { get; set; }

        [StringLength(50)]
        public string AmountSpent { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        [StringLength(150)]
        public string TypeOfCampaign { get; set; }

        public bool IsActive { get; set; }

        public DateTime? DateCreated { get; set; }

        [StringLength(100)]
        public string UserCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [StringLength(100)]
        public string UserModified { get; set; }

        [StringLength(50)]
        public string Incentive { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Lead> Lead { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LeadSource> LeadSource { get; set; }
    }
}
