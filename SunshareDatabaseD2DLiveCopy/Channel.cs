namespace SunshareDatabaseD2DLiveCopy
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Channel")]
    public partial class Channel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Channel()
        {
            Lead = new HashSet<Lead>();
        }

        public int ChannelId { get; set; }

        public int? ParentChannelId { get; set; }

        [StringLength(50)]
        public string ChannelName { get; set; }

        public bool IsActive { get; set; }

        public DateTime? DateCreated { get; set; }

        [StringLength(100)]
        public string UserCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [StringLength(100)]
        public string UserModified { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Lead> Lead { get; set; }
    }
}
