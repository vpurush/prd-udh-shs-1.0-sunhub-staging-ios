namespace SunshareDatabaseD2DLiveCopy
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DupliacteContractRequest")]
    public partial class DupliacteContractRequest
    {
        public int DupliacteContractRequestId { get; set; }

        public int LeadId { get; set; }

        [StringLength(250)]
        public string Reason { get; set; }

        public bool IsDecided { get; set; }

        public bool IsApproved { get; set; }

        public virtual Lead Lead { get; set; }
    }
}
