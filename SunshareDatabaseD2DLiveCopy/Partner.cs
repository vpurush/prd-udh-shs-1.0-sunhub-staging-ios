namespace SunshareDatabaseD2DLiveCopy
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Partner")]
    public partial class Partner
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Partner()
        {
            AspNetUsers = new HashSet<AspNetUsers>();
        }

        public int PartnerId { get; set; }

        [StringLength(20)]
        public string PartnerNumber { get; set; }

        [StringLength(50)]
        public string PartnerName { get; set; }

        [StringLength(250)]
        public string PartnerLogoUrl { get; set; }

        public bool IsActive { get; set; }

        public DateTime? DateCreated { get; set; }

        [StringLength(100)]
        public string UserCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [StringLength(100)]
        public string UserModified { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AspNetUsers> AspNetUsers { get; set; }
    }
}
