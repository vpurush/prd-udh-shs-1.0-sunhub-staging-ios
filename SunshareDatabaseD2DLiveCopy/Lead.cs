namespace SunshareDatabaseD2DLiveCopy
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Lead")]
    public partial class Lead
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Lead()
        {
            CallLog = new HashSet<CallLog>();
            DupliacteContractRequest = new HashSet<DupliacteContractRequest>();
            LeadUserMappingHistory = new HashSet<LeadUserMappingHistory>();
        }

        public int LeadId { get; set; }

        [StringLength(50)]
        public string PrimaryEmail { get; set; }

        [StringLength(50)]
        public string SecondaryEmail { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string DateOfBirth { get; set; }

        [StringLength(50)]
        public string UtilityAccountNo { get; set; }

        [StringLength(100)]
        public string UtilityCompanyName { get; set; }

        [StringLength(25)]
        public string HomePhoneNo { get; set; }

        [StringLength(25)]
        public string WorkPhoneNo { get; set; }

        [StringLength(25)]
        public string MobileNo { get; set; }

        public int ServiceAddressId { get; set; }

        public int BillingAddressId { get; set; }

        [StringLength(25)]
        public string RefferedBy { get; set; }

        public int SubscriptionPercentage { get; set; }

        [StringLength(25)]
        public string SSAType { get; set; }

        public int CampaignId { get; set; }

        public int LeadRankingId { get; set; }

        public int? LeadSourceId { get; set; }

        [StringLength(50)]
        public string EnvelopeId { get; set; }

        public bool IsActive { get; set; }

        public DateTime? DateCreated { get; set; }

        [StringLength(100)]
        public string UserCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [StringLength(100)]
        public string UserModified { get; set; }

        public int? LeadStatus_LeadStatusId { get; set; }

        public int? AspNetUsersId { get; set; }

        public int? ChannelId { get; set; }

        [StringLength(250)]
        public string ReasonToClose { get; set; }

        public bool IsAssigned { get; set; }

        public bool AllowDuplicateContract { get; set; }

        public virtual Address Address { get; set; }

        public virtual Address Address1 { get; set; }

        public virtual AspNetUsers AspNetUsers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CallLog> CallLog { get; set; }

        public virtual Campaign Campaign { get; set; }

        public virtual Channel Channel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DupliacteContractRequest> DupliacteContractRequest { get; set; }

        public virtual LeadRanking LeadRanking { get; set; }

        public virtual LeadSource LeadSource { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LeadUserMappingHistory> LeadUserMappingHistory { get; set; }
    }
}
