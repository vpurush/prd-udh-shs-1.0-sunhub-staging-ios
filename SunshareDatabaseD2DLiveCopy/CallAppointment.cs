namespace SunshareDatabaseD2DLiveCopy
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CallAppointment")]
    public partial class CallAppointment
    {
        public int CallAppointmentId { get; set; }

        public DateTime DateToCall { get; set; }

        public DateTime? CallDate { get; set; }

        public int CallLogId { get; set; }

        public DateTime? DateCreated { get; set; }

        [StringLength(100)]
        public string UserCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [StringLength(100)]
        public string UserModified { get; set; }

        public virtual CallLog CallLog { get; set; }
    }
}
