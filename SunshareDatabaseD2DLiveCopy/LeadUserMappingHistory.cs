namespace SunshareDatabaseD2DLiveCopy
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LeadUserMappingHistory")]
    public partial class LeadUserMappingHistory
    {
        public int LeadUserMappingHistoryId { get; set; }

        public int LeadId { get; set; }

        public int? AspNetUserId { get; set; }

        public DateTime? DateCreated { get; set; }

        [StringLength(100)]
        public string UserCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [StringLength(100)]
        public string UserModified { get; set; }

        public virtual AspNetUsers AspNetUsers { get; set; }

        public virtual Lead Lead { get; set; }
    }
}
