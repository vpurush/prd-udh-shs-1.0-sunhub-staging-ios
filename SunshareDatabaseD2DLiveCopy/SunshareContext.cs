namespace SunshareDatabaseD2DLiveCopy
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SunshareContext : DbContext
    {
        public SunshareContext()
            : base("name=SunshareContext")
        {
        }

        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<Address> Address { get; set; }
        public virtual DbSet<AddressSource> AddressSource { get; set; }
        public virtual DbSet<AddressUserMapping> AddressUserMapping { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<Audit> Audit { get; set; }
        public virtual DbSet<CallAppointment> CallAppointment { get; set; }
        public virtual DbSet<CallLog> CallLog { get; set; }
        public virtual DbSet<CallStatus> CallStatus { get; set; }
        public virtual DbSet<Campaign> Campaign { get; set; }
        public virtual DbSet<Channel> Channel { get; set; }
        public virtual DbSet<County> County { get; set; }
        public virtual DbSet<DupliacteContractRequest> DupliacteContractRequest { get; set; }
        public virtual DbSet<Lead> Lead { get; set; }
        public virtual DbSet<LeadLeadStatusMapping> LeadLeadStatusMapping { get; set; }
        public virtual DbSet<LeadRanking> LeadRanking { get; set; }
        public virtual DbSet<LeadSource> LeadSource { get; set; }
        public virtual DbSet<LeadSourceSubCategory> LeadSourceSubCategory { get; set; }
        public virtual DbSet<LeadStatus> LeadStatus { get; set; }
        public virtual DbSet<LeadUserMappingHistory> LeadUserMappingHistory { get; set; }
        public virtual DbSet<Partner> Partner { get; set; }
        public virtual DbSet<State> State { get; set; }
        public virtual DbSet<UserType> UserType { get; set; }
        public virtual DbSet<VisitStatus> VisitStatus { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>()
                .Property(e => e.AppartmentNo)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.AddressInformation)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.Zipcode)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.County)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.AddressUserMapping)
                .WithRequired(e => e.Address)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.Lead)
                .WithRequired(e => e.Address)
                .HasForeignKey(e => e.BillingAddressId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.Lead1)
                .WithRequired(e => e.Address1)
                .HasForeignKey(e => e.ServiceAddressId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AddressSource>()
                .Property(e => e.AddressSourceName)
                .IsUnicode(false);

            modelBuilder.Entity<AddressSource>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<AddressSource>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<AddressUserMapping>()
                .Property(e => e.Comments)
                .IsUnicode(false);

            modelBuilder.Entity<AddressUserMapping>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<AddressUserMapping>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetRoles>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetRoles>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetRoles>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetRoles>()
                .HasMany(e => e.AspNetUserRoles)
                .WithRequired(e => e.AspNetRoles)
                .HasForeignKey(e => e.RoleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUserClaims>()
                .Property(e => e.ClaimType)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserClaims>()
                .Property(e => e.ClaimValue)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserClaims>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserClaims>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserLogins>()
                .Property(e => e.LoginProvider)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserLogins>()
                .Property(e => e.ProviderKey)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserLogins>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserLogins>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserRoles>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserRoles>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUsers>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUsers>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AddressUserMapping)
                .WithOptional(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUsers>()
                .HasOptional(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUsers);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserRoles)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.LeadUserMappingHistory)
                .WithOptional(e => e.AspNetUsers)
                .HasForeignKey(e => e.AspNetUserId);

            modelBuilder.Entity<Audit>()
                .Property(e => e.TableName)
                .IsUnicode(false);

            modelBuilder.Entity<Audit>()
                .Property(e => e.ChangedColumns)
                .IsUnicode(false);

            modelBuilder.Entity<Audit>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<Audit>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<CallAppointment>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<CallAppointment>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<CallLog>()
                .Property(e => e.CallComment)
                .IsUnicode(false);

            modelBuilder.Entity<CallLog>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<CallLog>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<CallLog>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<CallLog>()
                .HasOptional(e => e.CallAppointment)
                .WithRequired(e => e.CallLog);

            modelBuilder.Entity<CallLog>()
                .HasMany(e => e.CallLog1)
                .WithOptional(e => e.CallLog2)
                .HasForeignKey(e => e.Call_CallLogId);

            modelBuilder.Entity<CallStatus>()
                .Property(e => e.CallStatusName)
                .IsUnicode(false);

            modelBuilder.Entity<CallStatus>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<CallStatus>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<CallStatus>()
                .HasMany(e => e.CallLog)
                .WithRequired(e => e.CallStatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Campaign>()
                .Property(e => e.CampaignName)
                .IsUnicode(false);

            modelBuilder.Entity<Campaign>()
                .Property(e => e.AmountSpent)
                .IsUnicode(false);

            modelBuilder.Entity<Campaign>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Campaign>()
                .Property(e => e.TypeOfCampaign)
                .IsUnicode(false);

            modelBuilder.Entity<Campaign>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<Campaign>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<Campaign>()
                .Property(e => e.Incentive)
                .IsUnicode(false);

            modelBuilder.Entity<Campaign>()
                .HasMany(e => e.Lead)
                .WithRequired(e => e.Campaign)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Campaign>()
                .HasMany(e => e.LeadSource)
                .WithMany(e => e.Campaign)
                .Map(m => m.ToTable("LeadSourceCampaign"));

            modelBuilder.Entity<Channel>()
                .Property(e => e.ChannelName)
                .IsUnicode(false);

            modelBuilder.Entity<Channel>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<Channel>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<County>()
                .Property(e => e.CountyName)
                .IsUnicode(false);

            modelBuilder.Entity<County>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<County>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<DupliacteContractRequest>()
                .Property(e => e.Reason)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.PrimaryEmail)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.SecondaryEmail)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.DateOfBirth)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.UtilityAccountNo)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.UtilityCompanyName)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.HomePhoneNo)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.WorkPhoneNo)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.MobileNo)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.RefferedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.SSAType)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.EnvelopeId)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.ReasonToClose)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .HasMany(e => e.CallLog)
                .WithRequired(e => e.Lead)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lead>()
                .HasMany(e => e.DupliacteContractRequest)
                .WithRequired(e => e.Lead)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lead>()
                .HasMany(e => e.LeadUserMappingHistory)
                .WithRequired(e => e.Lead)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LeadLeadStatusMapping>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<LeadLeadStatusMapping>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<LeadRanking>()
                .Property(e => e.RankingName)
                .IsUnicode(false);

            modelBuilder.Entity<LeadRanking>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<LeadRanking>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<LeadRanking>()
                .HasMany(e => e.Lead)
                .WithRequired(e => e.LeadRanking)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LeadSource>()
                .Property(e => e.LeadSourceName)
                .IsUnicode(false);

            modelBuilder.Entity<LeadSource>()
                .HasMany(e => e.LeadSourceSubCategory)
                .WithRequired(e => e.LeadSource)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LeadSourceSubCategory>()
                .Property(e => e.LeadSourceSubCategoryName)
                .IsUnicode(false);

            modelBuilder.Entity<LeadSourceSubCategory>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<LeadSourceSubCategory>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<LeadStatus>()
                .Property(e => e.StatusName)
                .IsUnicode(false);

            modelBuilder.Entity<LeadStatus>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<LeadStatus>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<LeadStatus>()
                .HasMany(e => e.LeadLeadStatusMapping)
                .WithRequired(e => e.LeadStatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LeadUserMappingHistory>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<LeadUserMappingHistory>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<Partner>()
                .Property(e => e.PartnerNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Partner>()
                .Property(e => e.PartnerName)
                .IsUnicode(false);

            modelBuilder.Entity<Partner>()
                .Property(e => e.PartnerLogoUrl)
                .IsUnicode(false);

            modelBuilder.Entity<Partner>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<Partner>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<Partner>()
                .HasMany(e => e.AspNetUsers)
                .WithRequired(e => e.Partner)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<State>()
                .Property(e => e.StateName)
                .IsUnicode(false);

            modelBuilder.Entity<State>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<State>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<UserType>()
                .Property(e => e.UserTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<UserType>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<UserType>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<UserType>()
                .HasMany(e => e.AspNetUsers)
                .WithRequired(e => e.UserType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VisitStatus>()
                .Property(e => e.VisitName)
                .IsUnicode(false);

            modelBuilder.Entity<VisitStatus>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<VisitStatus>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<VisitStatus>()
                .HasMany(e => e.AddressUserMapping)
                .WithRequired(e => e.VisitStatus)
                .WillCascadeOnDelete(false);
        }
    }
}
