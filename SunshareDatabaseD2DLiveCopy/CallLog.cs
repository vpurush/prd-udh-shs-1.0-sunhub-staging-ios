namespace SunshareDatabaseD2DLiveCopy
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CallLog")]
    public partial class CallLog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CallLog()
        {
            CallLog1 = new HashSet<CallLog>();
        }

        public int CallLogId { get; set; }

        public int LeadId { get; set; }

        [StringLength(400)]
        public string CallComment { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        public bool? IsDuplicateCall { get; set; }

        public int CallStatusId { get; set; }

        public DateTime? DateCreated { get; set; }

        [StringLength(100)]
        public string UserCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [StringLength(100)]
        public string UserModified { get; set; }

        public int? CallAppointmentId { get; set; }

        public int? Call_CallLogId { get; set; }

        public virtual CallAppointment CallAppointment { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CallLog> CallLog1 { get; set; }

        public virtual CallLog CallLog2 { get; set; }

        public virtual CallStatus CallStatus { get; set; }

        public virtual Lead Lead { get; set; }
    }
}
