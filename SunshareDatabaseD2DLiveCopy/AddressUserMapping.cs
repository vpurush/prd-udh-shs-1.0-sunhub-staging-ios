namespace SunshareDatabaseD2DLiveCopy
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AddressUserMapping")]
    public partial class AddressUserMapping
    {
        public int AddressUserMappingId { get; set; }

        public int AddressId { get; set; }

        public int? UserId { get; set; }

        public DateTime? DateToVisit { get; set; }

        public DateTime? DateVisited { get; set; }

        public int VisitStatusId { get; set; }

        public DateTime? AppointmentDate { get; set; }

        public TimeSpan AppointmentTime { get; set; }

        [StringLength(250)]
        public string Comments { get; set; }

        public bool IsActive { get; set; }

        public DateTime? DateCreated { get; set; }

        [StringLength(100)]
        public string UserCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [StringLength(100)]
        public string UserModified { get; set; }

        public virtual Address Address { get; set; }

        public virtual AspNetUsers AspNetUsers { get; set; }

        public virtual VisitStatus VisitStatus { get; set; }
    }
}
