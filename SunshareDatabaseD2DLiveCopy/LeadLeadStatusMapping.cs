namespace SunshareDatabaseD2DLiveCopy
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LeadLeadStatusMapping")]
    public partial class LeadLeadStatusMapping
    {
        public int LeadLeadStatusMappingId { get; set; }

        public int LeadStatusId { get; set; }

        public DateTime UpdatedDate { get; set; }

        public bool IsCurrent { get; set; }

        public DateTime? DateCreated { get; set; }

        [StringLength(100)]
        public string UserCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [StringLength(100)]
        public string UserModified { get; set; }

        public int LeadId { get; set; }

        public virtual LeadStatus LeadStatus { get; set; }
    }
}
