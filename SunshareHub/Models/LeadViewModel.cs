﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SunshareHub.Models
{
    public class LeadViewModel
    {
        [Required]
        [DisplayName("First name")]
        public string FirstName { get; set; }
        [Required]
        [DisplayName("Last name")]
        public string LastName { get; set; }
        [DisplayName("Service Address")]
        public string ServiceAddress { get;set; }
        public int Phone { get; set; }
        public IQueryable<SelectListItem> States { get; set; }
        [DisplayName("State")]
        public int StateId { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        [DisplayName("Zip Code")]
        public int ZipCode { get; set; }
        [DisplayName("Contract Status")]
        public int LeadStausId { get; set; }
        public string Email { get; set; }
        public IQueryable<SelectListItem> Subscriptions { get; set; }
        public string Subscription { get; set; }
        [DisplayName("Utility Account Number")]
        public string UtilityAccountNumber { get; set; }
        [DisplayName("Utility Provider")]
        public string UtilityProvider { get; set; }
        public IQueryable<SelectListItem> Campaigns { get; set; }
        [DisplayName("Campaign")]
        public int CampaignId { get; set; }
        [DisplayName("Refered By")]
        public string ReferedBy { get; set; }
        public IQueryable<SelectListItem> LeadSources { get; set; }
        [DisplayName("Lead Source")]
        public int LeadSourceId { get; set; }
        public int SalesRepId { get; set; }
        [DisplayName("Date Of Birth")]
        public DateTime DateOfBirth { get; set; }
    }
}
