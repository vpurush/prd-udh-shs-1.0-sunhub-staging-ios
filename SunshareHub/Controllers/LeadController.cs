﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SunshareDatabase;
using SunshareHub.ModelAssembler;
using SunshareHub.Models;

namespace SunshareHub.Controllers
{
    public class LeadController : Controller
    {
        private static readonly SunshareDatabaseContext Context = new SunshareDatabaseContext();
        readonly LeadViewModelAssembler _assembler = new LeadViewModelAssembler(Context);
        // GET: Lead
        public ActionResult Index()
        {
            return View(_assembler.Assemble(6));
        }

        public ActionResult Save(LeadViewModel model)
        {
            return View("~/Views/Dashboard/Dashboard.cshtml");
        }
    }
}