﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SunshareHub.Controllers
{
    [Authorize]
   public class SunshareHomeController : Controller
    {
        // GET: SunshareHome
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Home()
        {
            return View();
        }
    }
}