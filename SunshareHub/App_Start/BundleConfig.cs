﻿using System.Web;
using System.Web.Optimization;

namespace SunshareHub
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/select2.js",
                        "~/Scripts/DynamicScript.js",
                        "~/Scripts/jquery-ui-1.11.1.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                                 "~/Scripts/jquery.validate*",
                                 "~/Scripts/jquery.unobtrusive-ajax.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryCustom").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui-1.11.1.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                "~/Scripts/Custom/vendor/jquery/dist/jquery.min.js",
                "~/Scripts/Custom/vendor/jquery-ui/jquery-ui.mim.js",
                "~/Scripts/Custom/vendor/slimScroll/jquery.slimscroll.min.js",
                "~/Scripts/Custom/vendor/bootstrap/dist/js/bootstrap.min.js",
                "~/Scripts/Custom/vendor/jquery-flot/jquery.flot.js",
                "~/Scripts/Custom/vendor/jquery-flot/jquery.flot.resize.js",
                "~/Scripts/Custom/vendor/jquery-flot/jquery.flot.pie.js",
                "~/Scripts/Custom/vendor/flot.curvedlines/curvedLines.js",
                "~/Scripts/Custom/vendor/jquery.flot.spline/index.js",
                "~/Scripts/Custom/vendor/metisMenu/dist/metisMenu.min.js",
                "~/Scripts/Custom/vendor/iCheck/icheck.min.js",
                "~/Scripts/Custom/vendor/peity/jquery.peity.min.js",
                "~/Scripts/Custom/vendor/sparkline/index.js",
                "~/Scripts/Custom/vendor/datatables/media/js/jquery.dataTables.min.js",
                "~/Scripts/Custom/vendor/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js",
                "~/Scripts/Custom/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js",
                "~/Scripts/Custom/scripts/Homer.js",
                "~/Scripts/Custom/scripts/Charts.js",
                "~/Scripts/custom/scripts/mfb.js"
                ));

            bundles.Add(new StyleBundle("~/Content/bundle/css").Include(
                    "~/Content/bootstrap.css",
                    "~/Content/DynamicStyle.css",
                    "~/Content/site.css",
                    "~/Content/Validation.css",
                    "~/Content/css/select2.css",
                    "~/Content/select2custom.css"));

            bundles.Add(new StyleBundle("~/Content/bundle/custom").Include(
                "~/Content/custom/fontawesome/css/font-awesome.css",
                "~/Content/custom/metisMenu/dist/metisMenu.css",
                "~/Content/custom/animate.css/animate.css",
                "~/Content/custom/bootstrap/dist/css/bootstrap.css",
                "~/Content/custom/fonts/pe-icon-7-stroke/css/helper.css",
                "~/Content/custom/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css",
                "~/Content/custom/styles/style.css",
                "~/Content/custom/styles/mfb.css"
                ));
        }
    }
}
