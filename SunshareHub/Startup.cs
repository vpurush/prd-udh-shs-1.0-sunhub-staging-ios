﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SunshareHub.Startup))]
namespace SunshareHub
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
