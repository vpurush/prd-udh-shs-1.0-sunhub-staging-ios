﻿using System.ServiceModel;
using System.ServiceModel.Web;
using SunshareCommon;

namespace SunshareServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ISunshareServices
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/ValidateLogin",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        UserDetails ValidateLogin(string emailId, string password, string partnerId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ForgotPassword",
            RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        PasswordReturn ForgotPassword(string emailId);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/ChangePassword",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        PasswordReturn ChangePassword(string emailId, string oldPassword, string newPassword);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetRepAnalysis",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        AnalysisReturn GetRepAnalysis(int salesRepId);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate =
                "/LogVisits",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        LogReturn LogVisits(int salesRepId, float lat, float longi, string cusAddress, string cusState, string cusCity,
            string cusZip, string cusAppointmentDate, string cusAppointmentTime, string cusFirstName, string cusLastName,
            string cusMobile, int visitStatusId, int rank, string notes, string cusEmailId, int logId,
            string cusSubsription,float dlat,float dlongi,bool isSendInfo,bool isContract);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/DeleteVisit", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DeleteLogReturn DeleteVisit(int logId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/SalesRepData",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        //SalesRepReturn SalesRepData(int salesRepId, float lat, float lon, int distance);
        SalesRepReturn SalesRepData(int salesRepId);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate =
                "/GenerateContract",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        ContractReturn GenerateContract(int logId, bool isContract, int salesRepId, float lat, float longi,
            string cusAddress, string cusCounty, string cusState, string cusCity, string cusZip, string cusFirstName,
            string cusLastName, string cusMobile, int visitStatusId, int rank, string notes, string cusEmailId,
            string cusDob, string cusUtilityAccountNo, string cusSubsription,float dlat,float dlongi);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetEnvelopeStatus",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetEnvelopeStatus(string envelopeId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/UpdateLeadStatus",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        void UpdateLeadStatus(string envelopeId);
        //TODO: Profile pic upload method


        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/UploadFile?userId={userId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        FileUploadReturn UploadFile(int userId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/EnrollLead", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
       ContractReturn EnrollLead(string cusFirstName, string cusLastName, string cusEmailId, string cusMobile, string cusAddress,string cusAptNo, string cusCity, string cusState, 
            string cusZip,string cusCounty, string cusSubsription,string dateOfBirth, string cusUtilityAccountNo,bool isAutoEnrollment,string landingPageName);
    }    
}
