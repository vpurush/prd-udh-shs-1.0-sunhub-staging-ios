﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Hosting;
using System.Web.Security;
using SunshareDatabase;
using System.IO;
using System.Web;
using SunshareCommon;
using System.ServiceModel.Activation;
using SunshareDatabaseD2DLiveCopy;
using System.Diagnostics;
using System.Data.SqlClient;

namespace SunshareServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class SunshareServices : ISunshareServices
    {
        //readonly SunshareDatabaseContext _context = new SunshareDatabaseContext();
        readonly SunshareContext _context=new SunshareContext();
        readonly UserDetails _userDetail = new UserDetails() { ReturnCode = 401, ReturnMsg = "No data", PartnerLogoPic = string.Empty, ProfilePic = string.Empty };
        private readonly PasswordReturn _passwordReturn = new PasswordReturn() { ReturnCode = 401, ReturnMsg = "No Data" };
        readonly Methods _methods = new Methods();

        public UserDetails ValidateLogin(string emailId, string password, string partnerId)
        {
            //var intPartnerId = Convert.ToInt32(partnerId);

            if (!string.IsNullOrEmpty(emailId) && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(partnerId))
            {
                var partner = _context.Partner.FirstOrDefault(p => p.PartnerNumber == partnerId);
                if (partner != null)
                {
                    var intPartnerId = partner.PartnerId;
                    if (!_context.AspNetUsers.Any()) return _userDetail;
                    if (!_context.AspNetUsers.Any(u => u.Email == emailId & u.IsActive))
                    {
                        _userDetail.ReturnMsg = Messages.ValidEmailId;
                        return _userDetail;
                    }
                    else if (!_context.AspNetUsers.Any(u => u.Email == emailId && u.PasswordHash == password & u.IsActive))
                    {
                        _userDetail.ReturnMsg = Messages.ValidPassword;
                        return _userDetail;
                    }
                    else if (
                        !_context.AspNetUsers.Any(
                            u => u.Email == emailId && u.PasswordHash == password && u.PartnerId == intPartnerId))
                    {
                        _userDetail.ReturnMsg = Messages.ValidPartnerId;
                        return _userDetail;
                    }
                    var userInfo =
                        _context.AspNetUsers.FirstOrDefault(
                            u => u.Email == emailId && u.PasswordHash == password && u.PartnerId == intPartnerId & u.IsActive);
                    if (userInfo != null)
                    {
                        //_context.SaveChanges();

                        return new UserDetails
                        {
                            Name = $"{userInfo.FirstName} {userInfo.LastName}",
                            ProfilePic = string.IsNullOrEmpty(userInfo.ImageUrl) ? string.Empty : userInfo.ImageUrl,
                            PartnerLogoPic = VirtualPathUtility.ToAbsolute($"~/FileServer/{partner.PartnerLogoUrl}"),
                            ReturnCode = 200,
                            ReturnMsg = Messages.ValidationSuccess,
                            SaleRepId = userInfo.Id
                        };
                    }
                }
                else
                {
                    _userDetail.ReturnMsg = Messages.ValidPartnerId;
                    return _userDetail;
                }
            }
            _userDetail.ReturnMsg = Messages.ValidEmailandPassword;
            return _userDetail;
        }



        public PasswordReturn ForgotPassword(string emailId)
        {
            if (!string.IsNullOrEmpty(emailId))
            {
                if (!_context.AspNetUsers.Any()) return _passwordReturn;
                if (!_context.AspNetUsers.Any(u => u.Email == emailId & u.IsActive))
                {
                    _passwordReturn.ReturnMsg = Messages.ValidEmailId;
                    return _passwordReturn;
                }
                else
                {
                    var user = _context.AspNetUsers.FirstOrDefault(u => u.Email == emailId & u.IsActive);
                    if (user == null) return _passwordReturn;
                    user.PasswordHash = Membership.GeneratePassword(6, 0);
                    _context.SaveChanges();
                    var filePath = HostingEnvironment.MapPath("~/FileServer/Upload/PasswordResetTemplate.html");
                    if (filePath == null) return _passwordReturn;
                    var mailTemplate = File.ReadAllText(filePath).Replace("{0}", user.Email).Replace("{1}", user.PasswordHash);
                    //var mailMessage = string.Format(mailTemplate, user.Email, user.Password);
                    if (Methods.SendEmail(emailId, Convert.ToString(mailTemplate), Messages.EmailSubjectPasswordReset, true))
                    {
                        _passwordReturn.ReturnCode = 200;
                        _passwordReturn.ReturnMsg = Messages.PasswordResetSuccess;
                    }
                    else
                    {
                        _passwordReturn.ReturnCode = 401;
                        _passwordReturn.ReturnMsg = Messages.MailSendingError;
                    }
                    return _passwordReturn;
                }
            }
            _passwordReturn.ReturnMsg = Messages.ValidEmailId;
            return _passwordReturn;
        }

        public PasswordReturn ChangePassword(string emailId, string oldPassword, string newPassword)
        {
            if (!string.IsNullOrEmpty(emailId) && !string.IsNullOrEmpty(oldPassword))
            {
                if (!_context.AspNetUsers.Any()) return _passwordReturn;
                if (_context.AspNetUsers.Any(u => u.Email == emailId & u.IsActive))
                {
                    if (!_context.AspNetUsers.Any(u => u.Email == emailId && u.PasswordHash == oldPassword & u.IsActive))
                    {
                        _passwordReturn.ReturnMsg = Messages.ValidPassword;
                        return _passwordReturn;
                    }
                    else
                    {
                        var userinfo =
                            _context.AspNetUsers.FirstOrDefault(u => u.Email == emailId && u.PasswordHash == oldPassword & u.IsActive);
                        if (userinfo != null)
                        {
                            userinfo.PasswordHash = newPassword;
                            _context.SaveChanges();
                            _passwordReturn.ReturnCode = 200;
                            _passwordReturn.ReturnMsg = Messages.PasswordChangedSuccess;
                            return _passwordReturn;
                        }
                    }
                }
            }
            _passwordReturn.ReturnMsg = Messages.ValidEmailId;
            return _passwordReturn;
        }

        public AnalysisReturn GetRepAnalysis(int salesRepId)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            Stopwatch stopwatch1 = Stopwatch.StartNew();
            var analysis = new AnalysisReturn() { ReturnMsg = "Enter valid representative Id", ReturnCode = 401 };
            if (salesRepId == 0) return analysis;
            //_methods.UpdateDocuSignStatus(salesRepId);
            //stopwatch.Stop();
            //LogTimeTaken(DateTime.Now.ToString() + " Time Taken for Docusign Update GRA " +stopwatch.ElapsedMilliseconds.ToString());
            //stopwatch.Restart();
            var rep = _context.AspNetUsers.FirstOrDefault(u => u.Id == salesRepId & u.IsActive);
            var userLeads = rep.Lead.Where(l => l.IsActive);// _context.Lead.Where(l => l.AspNetUsersId == rep.Id & l.IsActive);//.Take(15);
            var userAppointments =
                _context.AddressUserMapping.Where(a => a.UserId == rep.Id & a.VisitStatusId == LeadVisitStatus.Appointment & a.IsActive).AsEnumerable();
            analysis.FireLeads =
                userLeads.Count(l=>l.LeadRankingId== LeadRankings.Fire);
            stopwatch.Stop();
            LogTimeTaken(DateTime.Now.ToString() + " Time Taken for FireLead Count GRA " + stopwatch.ElapsedMilliseconds.ToString());
            stopwatch.Restart();
            //var count = 0;
            //foreach (var lead in userLeads)
            //{
            //    var leadLeadStatusMapping = _context.LeadLeadStatusMapping.FirstOrDefault(l => l.IsCurrent & l.LeadId == lead.LeadId);
            //    if (leadLeadStatusMapping?.LeadStatusId == LeadContractStatus.ContractSent)
            //        count = count + 1;
            //}
            analysis.TodatOutstandingContract = _context.Database.SqlQuery<int>("GetAgentOutStandingContractCount @salesRep", new SqlParameter("salesRep", salesRepId)).FirstOrDefault(); //count;//userLeads.Count(l => l.GetLeadStatusId == LeadContractStatus.ContractSent);
            stopwatch.Stop();
            LogTimeTaken(DateTime.Now.ToString() + " Time Taken for Outstanding Contract Count GRA " + stopwatch.ElapsedMilliseconds.ToString());
            stopwatch.Restart();
            analysis.WeekPeriod = $"{DateTime.Now.StartOfWeekAsShortDateString(DayOfWeek.Monday)} to {DateTime.Now.ToShortDateString()}";
            analysis.TodayAppointment =
               userAppointments.Count(u => u.AppointmentDate.IsToday());
            stopwatch.Stop();
            LogTimeTaken(DateTime.Now.ToString() + " Time Taken for Today Appointment Count GRA " + stopwatch.ElapsedMilliseconds.ToString());
            stopwatch.Restart();
            LogTimeTaken(DateTime.Now.StartOfWeek(DayOfWeek.Monday) + "Another" + DateTime.Now);
            analysis.WeeklyData = GetWeeklyData(DateTime.Now.StartOfWeek(DayOfWeek.Monday), DateTime.Now);
            stopwatch.Stop();
            LogTimeTaken(DateTime.Now.ToString() + " Time Taken for Weekly Data Count GRA " + stopwatch.ElapsedMilliseconds.ToString());
            //stopwatch.Restart();
            analysis.ReturnMsg = Messages.Success;
            analysis.ReturnCode = 200;
            stopwatch1.Stop();
            LogTimeTaken(DateTime.Now.ToString() + " Entire Method GRA " + stopwatch1.ElapsedMilliseconds.ToString());
            //stopwatch.Stop();
            return analysis;
        }

        public void UpdateLeadStatus(string envelopeId)
        {
            _methods.UpdateLeadStatus(_context.Lead.FirstOrDefault(l => l.EnvelopeId == envelopeId));
        }

        private IEnumerable<UserAnalysis> GetWeeklyData(DateTime start, DateTime end)
        {
            return _context.Database.SqlQuery<UserAnalysis>("GetWeeklyData @StartDate, @EndDate", new SqlParameter("StartDate", start), new SqlParameter("EndDate", end)).AsEnumerable();
            //var userAnalysis = new List<UserAnalysis>();
            //foreach (var user in _context.AspNetUsers.Where(u => u.UserTypeId == SalesUserTypes.D2D & u.IsActive)) //TODO: filter Using role.
            //{
            //    var userLeads = _context.AddressUserMapping.Where(a => a.UserId == user.Id & a.IsActive).ToList();
            //    //var knockedLeads = userLeads.Count(a => a.DateVisited.TimeBetween(start, end));
            //    var count = (from userAddress in userLeads
            //                 select _context.Lead.FirstOrDefault(l => l.IsActive & (l.ServiceAddressId == userAddress.AddressId || l.BillingAddressId == userAddress.AddressId)) into userLead
            //                 where userLead != null select userLead.LeadId).Count(leadId => _context.LeadLeadStatusMapping.Where(lm => lm.IsCurrent & lm.LeadId == leadId && lm.LeadStatusId == LeadContractStatus.ContractSigned).ToList().Any(a => a.DateModified.TimeBetween(start, end)));
            //    userAnalysis.Add(new UserAnalysis() { UserName = $"{user.FirstName}", Knocks = count });
            //}
            //return userAnalysis;
        }

        public LogReturn LogVisits(int salesRepId, float lat, float longi, string cusAddress, string cusState,
            string cusCity, string cusZip,
            string cusAppointmentDate, string cusAppointmentTime, string cusFirstName, string cusLastName,
            string cusMobile,
            int visitStatusId, int rank, string notes, string cusEmailId, int logId, string cusSubsription, float dlat, float dlongi,bool isSendInfo,bool isContract)
        {
            var logreturn = new BaseReturnWithLogId { ReturnMsg = "Error saving data", ReturnCode = 401, LogId = string.Empty };
            if (salesRepId == 0 || lat.Equals(0) || longi.Equals(0) || visitStatusId == 0)
            {
                return new LogReturn { ReturnMsg = Messages.ErrorProvideRequiredValues, ReturnCode = 401 };
            }
            if (logId != 0)
            {
                UpdateLog(salesRepId, lat, longi, cusAddress, cusCity, cusZip, cusFirstName, cusLastName, cusMobile, visitStatusId, rank, notes, cusEmailId, logId, cusState, logreturn, dlat, dlongi, cusSubsription, cusAppointmentTime, cusAppointmentDate);
                if (!isSendInfo)
                    return new LogReturn { ReturnCode = 200, ReturnMsg = Messages.Success, LogId = Convert.ToString(logId) };
                else
                    return _methods.SendInformation(_context.Lead.FirstOrDefault(l=>l.ServiceAddressId==_context.AddressUserMapping.FirstOrDefault(au=>au.AddressUserMappingId==logId).AddressId));             
            }
            if (_context.Address.Any(a => a.Longitude.Equals(longi) & a.Lattitude.Equals(lat) & a.IsActive))
                return new LogReturn { ReturnCode = 401, ReturnMsg = "Address Already Present" };
            BaseReturnWithLogId logVisits;
            return SaveLog(salesRepId, lat, longi, cusAddress, cusCity, cusZip, cusFirstName, cusLastName, cusMobile, visitStatusId, rank, notes, cusEmailId, cusState,logreturn, out logVisits, dlat, dlongi, LeadAddressSources.D2D, cusSubsription, cusAppointmentDate, cusAppointmentTime) ? 
               (!isSendInfo)? new LogReturn { ReturnMsg = logVisits.ReturnMsg, ReturnCode = logVisits.ReturnCode, LogId = logVisits.LogId }: _methods.SendInformation(_context.Lead.FirstOrDefault(l => l.ServiceAddressId == _context.AddressUserMapping.FirstOrDefault(au => au.AddressUserMappingId.ToString() == logVisits.LogId).AddressId))            
                    : (!isSendInfo)?new LogReturn { ReturnMsg = logreturn.ReturnMsg, ReturnCode = logreturn.ReturnCode, LogId = logreturn.LogId }: _methods.SendInformation(_context.Lead.FirstOrDefault(l => l.ServiceAddressId == _context.AddressUserMapping.FirstOrDefault(au => au.AddressUserMappingId.ToString() == logreturn.LogId).AddressId));

        }

        public DeleteLogReturn DeleteVisit(int logId)
        {

            var mapping = _context.AddressUserMapping.FirstOrDefault(a => a.AddressUserMappingId == logId & a.IsActive);
            if (mapping == null) return new DeleteLogReturn { ReturnMsg = Messages.RecordNotFound, ReturnCode = 401 };
            {
                var addressId = mapping.AddressId;
                var userAddress = _context.Address.FirstOrDefault(a => a.AddressId == addressId & a.IsActive);
                var userLead = _context.Lead.FirstOrDefault(
                    s => (s.ServiceAddressId == addressId || s.BillingAddressId == addressId) & s.IsActive);
                using (var transactionContext = _context.Database.BeginTransaction())
                {
                    try
                    {
                        mapping.IsActive = false;
                        if (userAddress != null) userAddress.IsActive = false;
                        if (userLead != null) userLead.IsActive = false;
                        _context.SaveChanges();
                        transactionContext.Commit();
                        return new DeleteLogReturn { ReturnMsg = Messages.Success, ReturnCode = 200 };
                    }
                    catch (Exception)
                    {
                        transactionContext.Rollback();
                        return new DeleteLogReturn { ReturnMsg = Messages.ErrorDeletingRecord, ReturnCode = 401 };
                        /*
                                                throw ex;
                        */
                    }
                }
            }
        }

        private void UpdateLog(int salesRepId, float lat, float longi, string cusAddress, string cusCity, string cusZip, string cusFirstName, string cusLastName, string cusMobile, int visitStatusId, int rank, string notes, string cusEmailId, int logId, string cusState, BaseReturnWithLogId logreturn, float dlat, float dlongi, string cusSubsription = null, string cusAppointmentTime = null, string cusAppointmentDate = null, string cusDob = null, string cusUtilityAccountNo = null, string cusCounty = null)
        {
            if (logreturn == null) throw new ArgumentNullException(nameof(logreturn));
            using (var transactionContext = _context.Database.BeginTransaction())
            {
                try
                {
                    var mapping = _context.AddressUserMapping.FirstOrDefault(a => a.AddressUserMappingId == logId & a.IsActive);
                    var userAddress = _context.Address.FirstOrDefault(a => a.AddressId == mapping.AddressId & a.IsActive);
                    var userLead = _context.Lead.FirstOrDefault(
                        s => (s.ServiceAddressId == mapping.AddressId || s.BillingAddressId == mapping.AddressId) & s.IsActive);
                    if (userAddress != null)
                    {
                        if (!string.IsNullOrEmpty(cusAddress))
                            userAddress.AddressInformation = cusAddress;
                        if (!string.IsNullOrEmpty(cusCity))
                            userAddress.City = cusCity;
                        if (!string.IsNullOrEmpty(cusZip))
                            userAddress.Zipcode = cusZip;
                        if (!string.IsNullOrEmpty(cusState))
                            userAddress.State = cusState;
                        userAddress.Longitude = longi;
                        userAddress.Lattitude = lat;
                        userAddress.DeviceLat = dlat;
                        userAddress.DeviceLong = dlongi;
                        //userAddress.AddressSourceId = LeadAddressSources.D2D;
                        userAddress.DateModified = DateTime.Today;
                        if (!string.IsNullOrEmpty(cusCounty))
                            userAddress.County = cusCounty;
                    }
                    _context.SaveChanges();
                    if (userLead != null)
                    {
                        userLead.BillingAddressId = userAddress?.AddressId ?? userLead.BillingAddressId;
                        if (!string.IsNullOrEmpty(cusFirstName))
                            userLead.FirstName = cusFirstName;
                        if (!string.IsNullOrEmpty(cusLastName))
                            userLead.LastName = cusLastName;
                        if (!string.IsNullOrEmpty(cusMobile))
                            userLead.MobileNo = cusMobile;
                        userLead.AspNetUsersId = salesRepId;
                        userLead.LeadRankingId = rank == 0 ? LeadRankings.NoRanking : rank;
                        if (!string.IsNullOrEmpty(cusEmailId))
                            userLead.PrimaryEmail = cusEmailId;
                        //userLead.//CampaignId = 1; //TODO:needs change
                        //userLead.LeadStatusId = LeadContractStatus.LeadCreated;
                        userLead.ServiceAddressId = userAddress?.AddressId ?? userLead.ServiceAddressId;
                        if (!string.IsNullOrEmpty(cusDob))
                            userLead.DateOfBirth = cusDob;
                        if (!string.IsNullOrEmpty(cusUtilityAccountNo))
                            userLead.UtilityAccountNo = cusUtilityAccountNo;
                        if (!string.IsNullOrEmpty(cusSubsription))
                            userLead.SubscriptionPercentage = Convert.ToInt32(cusSubsription);
                        userLead.DateModified = DateTime.Today;
                    }
                    _context.SaveChanges();
                    if (mapping != null)
                    {
                        mapping.AddressId = userAddress?.AddressId ?? mapping.AddressId;
                        mapping.VisitStatusId = visitStatusId;
                        mapping.AppointmentDate = !string.IsNullOrEmpty(cusAppointmentDate)
                            ? Convert.ToDateTime(cusAppointmentDate)
                            : (DateTime?)null;
                        mapping.AppointmentTime = !string.IsNullOrEmpty(cusAppointmentTime)
                            ? TimeSpan.Parse(cusAppointmentTime)
                            : TimeSpan.Zero;
                        mapping.DateVisited = DateTime.Today;
                        mapping.IsActive = true;
                        mapping.UserId = salesRepId;
                        if (!string.IsNullOrEmpty(notes))
                            mapping.Comments = notes;
                        mapping.DateModified = DateTime.Today;
                    }
                    _context.SaveChanges();
                    transactionContext.Commit();
                    var rep = _context.AspNetUsers.FirstOrDefault(u => u.IsActive && u.Id == salesRepId);
                    if (visitStatusId == LeadVisitStatus.Appointment & userLead != null)
                    {
                        var filePath = HostingEnvironment.MapPath("~/FileServer/Upload/SendAppoinmentTemplate.html");
                        if (filePath != null)
                        {
                            var mailTemplate =
                                File.ReadAllText(filePath).Replace("{3}", (rep != null) ? $"{rep.FirstName} {rep.LastName}" : string.Empty)
                                .Replace("{4}",(rep!=null)?rep.Email:string.Empty).Replace("{5}", (rep != null) ? rep.PhoneNumber.ToString() : string.Empty)
                                    .Replace("{0}", userLead.FirstName)
                                    .Replace("{1}", userLead.LastName).Replace("{2}", $"{Convert.ToDateTime(cusAppointmentDate).Add(TimeSpan.Parse(mapping.AppointmentTime.ToString())):MM dd, yyyy}")
                                    .Replace("{7}", (rep != null) ? $"{rep.FirstName}":string.Empty).Replace("{6}", $"{Convert.ToDateTime(cusAppointmentDate).Add(TimeSpan.Parse(mapping.AppointmentTime.ToString())):hh:mm}");
                            // $"{Convert.ToDateTime(cusAppointmentDate): MMM d, yyyy}").Replace("{3}", mapping.AppointmentTime.ToString());
                            Methods.SendEmail(userLead.PrimaryEmail, mailTemplate, Messages.EmailAppointmentSubject, true);
                        }
                    }
                    //logreturn.ReturnMsg = Messages.Success;FileUpload
                    //logreturn.LogId = Convert.ToString(logId);
                    //logreturn.ReturnCode = 200;
                }
                catch (Exception)
                {
                    transactionContext.Rollback();
                    throw;
                }
            }
        }

        private
            bool SaveLog(int salesRepId, float lat, float longi, string cusAddress, string cusCity, string cusZip, string cusFirstName, string cusLastName, string cusMobile,
            int visitStatusId, int rank, string notes, string cusEmailId, string cusState, BaseReturnWithLogId logreturn, out BaseReturnWithLogId logVisits, float dlat, float dlongi, int addressSourceId, string cusSubsription = null, string cusAppointmentDate = null, string cusAppointmentTime = null, string cusDob = null, string cusUtilityAccountNo = null, string cusCounty = null)
        {
            using (var transactionContext = _context.Database.BeginTransaction())
            {
                try
                {
                    var userAddress = _context.Address.Add(new SunshareDatabaseD2DLiveCopy.Address()
                    {
                        AddressInformation = cusAddress,
                        City = cusCity,
                        Zipcode = cusZip,
                        State = cusState ?? string.Empty,
                        Longitude = longi,
                        Lattitude = lat,
                        AddressSourceId = addressSourceId,//LeadAddressSources.D2D,
                        County = cusCounty ?? string.Empty,
                        IsActive = true,
                        DeviceLong = dlongi,
                        DeviceLat = dlat,
                        DateCreated= DateTime.Today
                    });
                    _context.SaveChanges();
                    SunshareDatabaseD2DLiveCopy.Lead updateLead = null;
                    if (_context.Lead != null)
                    {
                        updateLead = new SunshareDatabaseD2DLiveCopy.Lead
                        {
                            BillingAddressId = userAddress.AddressId,
                            FirstName = cusFirstName,
                            LastName = cusLastName,
                            MobileNo = cusMobile,
                            AspNetUsersId = salesRepId,
                            PrimaryEmail = cusEmailId,
                            CampaignId = 1,
                            DateCreated = DateTime.Today,
                            //LeadStatusId = LeadContractStatus.LeadCreated,
                            ServiceAddressId = userAddress.AddressId,
                            IsActive = true,
                            LeadSourceId = 1
                        };
                        if (rank != 0)
                            updateLead.LeadRankingId = rank;
                        if (!string.IsNullOrEmpty(cusDob))
                            updateLead.DateOfBirth = cusDob;
                        if (!string.IsNullOrEmpty(cusUtilityAccountNo))
                            updateLead.UtilityAccountNo = cusUtilityAccountNo;
                        if (!string.IsNullOrEmpty(cusSubsription))
                            updateLead.SubscriptionPercentage = Convert.ToInt32(cusSubsription);
                        var userLead = _context.Lead.Add(updateLead);
                        _context.SaveChanges();
                        if (userLead == null) throw new ArgumentNullException(nameof(userLead));
                        var currentLeadLeadStatusMapping =
                            _context.LeadLeadStatusMapping.FirstOrDefault(
                                l => l.IsCurrent & l.LeadId == userLead.LeadId);
                        if (currentLeadLeadStatusMapping != null) currentLeadLeadStatusMapping.IsCurrent = false;
                        _context.LeadLeadStatusMapping.Add(new SunshareDatabaseD2DLiveCopy.LeadLeadStatusMapping()
                        {
                            IsCurrent = true,
                            LeadId = userLead.LeadId,
                            LeadStatusId = LeadContractStatus.LeadCreated,
                            UpdatedDate = DateTime.Today,
                            DateCreated = DateTime.Today
                        });

                    }
                    _context.SaveChanges();
                    if (_context.AddressUserMapping == null)
                    {
                        logVisits = logreturn;
                        return true;
                    }
                    var mapping =
                        _context.AddressUserMapping.Add(new SunshareDatabaseD2DLiveCopy.AddressUserMapping()
                        {
                            AddressId = userAddress.AddressId,
                            VisitStatusId = visitStatusId,
                            AppointmentDate =
                                !string.IsNullOrEmpty(cusAppointmentDate)
                                    ? Convert.ToDateTime(cusAppointmentDate)
                                    : (DateTime?)null,
                            AppointmentTime =
                                !string.IsNullOrEmpty(cusAppointmentTime) ? TimeSpan.Parse(cusAppointmentTime) : TimeSpan.Zero,
                            DateVisited = DateTime.Today,
                            IsActive = true,
                            UserId = salesRepId,
                            Comments = notes,
                            DateCreated = DateTime.Today
                        });
                    if (mapping == null) throw new ArgumentNullException(nameof(mapping));
                    _context.SaveChanges();
                    transactionContext.Commit();
                    var rep = _context.AspNetUsers.FirstOrDefault(u => u.IsActive && u.Id == salesRepId);
                    if (visitStatusId == LeadVisitStatus.Appointment & updateLead != null)
                    {
                        var filePath = HostingEnvironment.MapPath("~/FileServer/Upload/SendAppoinmentTemplate.html");
                        if (filePath != null)
                        {
                            var mailTemplate =
                                File.ReadAllText(filePath)
                                    .Replace("{0}", updateLead.FirstName).Replace("{3}", (rep!=null)? $"{rep.FirstName} {rep.LastName}" : string.Empty)
                                    .Replace("{4}", (rep != null) ? rep.Email : string.Empty).Replace("{5}", (rep != null) ? rep.PhoneNumber.ToString() : string.Empty)
                                    .Replace("{1}", updateLead.LastName).Replace("{2}", $"{Convert.ToDateTime(cusAppointmentDate).Add(TimeSpan.Parse(mapping.AppointmentTime.ToString())):MMM dd, yyyy}")
                                    .Replace("{7}", (rep != null) ? $"{rep.FirstName}" : string.Empty).Replace("{6}", $"{Convert.ToDateTime(cusAppointmentDate).Add(TimeSpan.Parse(mapping.AppointmentTime.ToString())):hh:mm}");
                            Methods.SendEmail(updateLead.PrimaryEmail, mailTemplate, Messages.EmailAppointmentSubject, true);
                        }
                    }
                    logreturn.ReturnMsg = Messages.Success;
                    logreturn.LogId = Convert.ToString(mapping.AddressUserMappingId);
                    logreturn.ReturnCode = 200;
                }
                catch (Exception)
                {
                    transactionContext.Rollback();
                    throw;
                }
            }
            logVisits = null;
            return false;
        }

        //public SalesRepReturn SalesRepData(int salesRepId,float lat,float lon,int distance)
        public SalesRepReturn SalesRepData(int salesRepId)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            Stopwatch stopwatch1 = Stopwatch.StartNew();
            var salesRepReturn = new SalesRepReturn { ReturnMsg = "Enter valid representative Id", ReturnCode = 401 };
            if (salesRepId == 0) return salesRepReturn;
                 salesRepReturn.AssociatedLeads = MapLeads(salesRepId);
                stopwatch.Stop();
                LogTimeTaken(DateTime.Now.ToString() + " Time Taken for Map Leads SRD " + stopwatch.ElapsedMilliseconds.ToString());
            salesRepReturn.ReturnCode = 200;
            salesRepReturn.ReturnMsg = Messages.Success;
            stopwatch1.Stop();
            LogTimeTaken(DateTime.Now.ToString() + " Entire Method SRD " + stopwatch1.ElapsedMilliseconds.ToString());            
            return salesRepReturn;
        }

        public ContractReturn GenerateContract(int logId, bool isContract, int salesRepId, float lat, float longi, string cusAddress, string cusCounty, string cusState, string cusCity, string cusZip, string cusFirstName, string cusLastName, string cusMobile, int visitStatusId, int rank, string notes, string cusEmailId, string cusDob, string cusUtilityAccountNo, string cusSubsription, float dlat, float dlongi)
        {

            var contractReturn = new BaseReturnWithLogId() { ReturnMsg = "Error saving data", ReturnCode = 401, LogId = string.Empty };
            if (salesRepId == 0 || lat.Equals(0) || longi.Equals(0) || visitStatusId == 0)
                return new ContractReturn() { ReturnMsg = Messages.ErrorProvideRequiredValues, ReturnCode = 401 };
            if (logId != 0)
            {
                UpdateLog(salesRepId, lat, longi, cusAddress, cusCity, cusZip, cusFirstName, cusLastName, cusMobile, visitStatusId, rank, notes, cusEmailId, logId, cusState, contractReturn, dlat, dlongi, cusSubsription, null, null, cusDob, cusUtilityAccountNo, cusCounty);
            }
            else
            {
                if (_context.Address.Any(a => a.Longitude.Equals(longi) & a.Lattitude.Equals(lat) & a.IsActive))
                    return new ContractReturn { ReturnCode = 401, ReturnMsg = "Address Already Present" };
                BaseReturnWithLogId logVisits;
                contractReturn = SaveLog(salesRepId, lat, longi, cusAddress, cusCity, cusZip, cusFirstName, cusLastName, cusMobile, visitStatusId, rank, notes, cusEmailId, cusState,
                    contractReturn, out logVisits, dlat, dlongi, LeadAddressSources.D2D, cusSubsription, null, null, cusDob, cusUtilityAccountNo, cusCounty)
                    ? new ContractReturn { ReturnMsg = logVisits.ReturnMsg, ReturnCode = logVisits.ReturnCode, LogId = logVisits.LogId } : new ContractReturn { ReturnMsg = contractReturn.ReturnMsg, ReturnCode = contractReturn.ReturnCode, LogId = contractReturn.LogId };
                logId = string.IsNullOrEmpty(contractReturn.LogId) ? 0 : Convert.ToInt32(contractReturn.LogId);
            }
            if (logId == 0) return new ContractReturn { ReturnMsg = "Error Saving data/ No data Found", ReturnCode = 401, LogId = "0" };
            var rep = _context.AspNetUsers.FirstOrDefault(u => u.Id == salesRepId & u.IsActive);
            var mapping = _context.AddressUserMapping.FirstOrDefault(a => a.AddressUserMappingId == logId & a.IsActive);
            if (_context.Lead.Any(l => l.IsActive & l.PrimaryEmail == cusEmailId & !string.IsNullOrEmpty(l.EnvelopeId)))
                return new ContractReturn { ReturnMsg = "Contract already sent / signed", ReturnCode = 401 };
            var userLead = _context.Lead.FirstOrDefault(
                s => (s.ServiceAddressId == mapping.AddressId & s.IsActive & s.PrimaryEmail == cusEmailId & string.IsNullOrEmpty(s.EnvelopeId)));// || s.EnvelopeId==string.Empty));
            var userAddress = _context.Address.FirstOrDefault(a => a.AddressId == userLead.ServiceAddressId);
            if (userLead == null) return new ContractReturn { ReturnMsg = "Something went wrong", ReturnCode = 401 };
            return isContract ? _methods.ContractReturn(userLead, userAddress, rep, mapping) : _methods.SendInformationC(userLead);
        }

        public string GetEnvelopeStatus(string envelopeId)
        {
            throw new NotImplementedException();
        }

        public FileUploadReturn UploadFile(int userId)
        {
            var filename = $"{Guid.NewGuid()}.jpeg";
            var result = new FileUploadReturn() { FileName = filename, UserId = userId, ReturnCode = 400, ReturnMsg = "File upload failed", Url = string.Empty };
            try
            {
                if (HttpContext.Current.Request.Files.Count == 0)
                    throw new InvalidOperationException("no files been received.");
                var image = HttpContext.Current.Request.Files[0].InputStream;

                var filePath = Path.Combine(HostingEnvironment.MapPath("~/FileServer/"), filename);
                using (var writer = new FileStream(filePath, FileMode.Create))
                {
                    int readCount;
                    var buffer = new byte[8192];
                    while ((readCount = image.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        writer.Write(buffer, 0, readCount);
                    }
                }
                if (userId != 0)
                {
                    var user = _context.AspNetUsers.FirstOrDefault(l => l.Id == userId);
                    user.ImageUrl = VirtualPathUtility.ToAbsolute($"~/FileServer/{filename}");
                    _context.SaveChanges();
                }
                else
                {
                    userId = Convert.ToInt32(HttpContext.Current.Request.Files[0].FileName.Split('.')[0]);
                    var user = _context.AspNetUsers.FirstOrDefault(l => l.Id == userId);
                    if (user != null) user.ImageUrl = VirtualPathUtility.ToAbsolute($"~/FileServer/{filename}");
                    _context.SaveChanges();
                }
                result.ReturnCode = 200;
                result.ReturnMsg = "File Upload Success";
                result.Url = VirtualPathUtility.ToAbsolute($"~/FileServer/{filename}");
                result.UserId = userId;
                return result;
            }
            catch (Exception exception)
            {

                Helper.LogError(exception);
                //throw Helper.ConvertToSoapFault(exception);
                return result;
            }

            //return result;

        }

        public ContractReturn EnrollLead(string cusFirstName, string cusLastName, string cusEmailId, string cusMobile, string cusAddress,
            string cusAptNo, string cusCity, string cusState, string cusZip, string cusCounty, string cusSubsription,
            string dateOfBirth, string cusUtilityAccountNo, bool isAutoEnrollment, string landingPageName)
        {
            var source = _context.LeadSource.FirstOrDefault(l => l.LeadSourceName == landingPageName & l.IsActive) ??
                         _context.LeadSource.Add(new SunshareDatabaseD2DLiveCopy.LeadSource { IsActive = true, LeadSourceName = landingPageName});
            BaseReturnWithLogId logVisits;
            var contractReturn = new ContractReturn { ReturnMsg = "Error saving data", ReturnCode = 401, LogId = string.Empty };
            contractReturn = SaveLog(0, 0, 0, cusAddress, cusCity, cusZip, cusFirstName, cusLastName, cusMobile, 0, 0, "Website/LandingPage", cusEmailId, cusState,
                contractReturn, out logVisits, 0, 0, LeadAddressSources.Website, cusSubsription, null, null, dateOfBirth, cusUtilityAccountNo, cusCounty)
                ? new ContractReturn { ReturnMsg = logVisits.ReturnMsg, ReturnCode = logVisits.ReturnCode, LogId = logVisits.LogId } : new ContractReturn { ReturnMsg = contractReturn.ReturnMsg, ReturnCode = contractReturn.ReturnCode, LogId = contractReturn.LogId };
            var logId = string.IsNullOrEmpty(contractReturn.LogId) ? 0 : Convert.ToInt32(contractReturn.LogId);
            if (logId == 0) return new ContractReturn { ReturnMsg = "Error Saving data/ No data Found", ReturnCode = 401, LogId = "0" };
            var rep = _context.AspNetUsers.FirstOrDefault(u => u.IsActive);
            var mapping = _context.AddressUserMapping.FirstOrDefault(a => a.AddressUserMappingId == logId & a.IsActive);
            if (_context.Lead.Any(l => l.IsActive & l.PrimaryEmail == cusEmailId & l.EnvelopeId != null))
                return new ContractReturn { ReturnMsg = "Contract already sent / signed", ReturnCode = 401 };
            var userLead = _context.Lead.FirstOrDefault(
                s => (s.ServiceAddressId == mapping.AddressId & s.IsActive & s.PrimaryEmail == cusEmailId & s.EnvelopeId == null));
            var userAddress = _context.Address.FirstOrDefault(a => a.AddressId == userLead.ServiceAddressId);
            if (userLead == null) return new ContractReturn { ReturnMsg = "Something went wrong", ReturnCode = 401 };
            if (!isAutoEnrollment) return new ContractReturn {ReturnMsg = "Lead Saved Successfully", ReturnCode = 200};
            if (rep == null) return contractReturn;
            userLead.AspNetUsersId = rep.Id;
            userLead.LeadSourceId = source.LeadSourceId;
            _context.SaveChanges();
            return _methods.ContractReturn(userLead, userAddress, rep, mapping,true);
        }
        //end of method
        
        //private ICollection<LeadInfo> MapLeads(int salesRepId,float lat,float lon,int distance
        private ICollection<LeadInfo> MapLeads(int salesRepId)
        {
            var result=_context.Database.SqlQuery<LeadInfo>("GetMappedLeadsPerAgent @salesRepId", new SqlParameter("salesRepId", salesRepId)).AsEnumerable();
            return result.ToList();
        }
        
        private void LogTimeTaken(string time)
        {   
            using (var Lfile = File.AppendText("C:/log_Stage.txt"))
            {
              Lfile.WriteLine(time);                
            }
        }

    }
   
}

