﻿using System.Web.Mvc;
using SunshareDatabase;
using SunshareHubOne.ModelAssembler;
using SunshareHubOne.Models;
using SunshareHubOne.ContextAssembler;
//using SunshareHubOne.sunshareservice;

namespace SunshareHubOne.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        readonly DashboardViewModelAssembler _assembler;
        readonly DashboardContextAssembler _contextAssembler;
       
        // GET: Dashboard
        public DashboardController()
        {
            _contextAssembler = new DashboardContextAssembler();
            var context = new SunshareDatabaseContext();
            _assembler = new DashboardViewModelAssembler(context);
        }
        [HttpGet]
        public ActionResult Index(int Id)
        {           
            ModelState.Clear();
            var model = _assembler.Assemble(Id);
            return View("Dashboard", model);
        }

        [HttpGet]
        public ActionResult IndexAdmin(int Id)
        {
            ModelState.Clear();
            var model = _assembler.Assemble(Id,true);
            return View("Dashboard", model);
        }

        [HttpGet]
        public ActionResult Manage(int Id)
        {
            ModelState.Clear();
            var model = _assembler.AssembleToManage(Id);
            return View("Manage", model);
        }

        public ActionResult LoadModelAction()
        {
            TempData["Search"] = TempData["Search"];
            TempData["SearchResults"] = TempData["SearchResults"];
            return View("Dashboard", (DashboardViewModel)TempData["SearchResults"]);
        }
        [HttpPost]
        //public ActionResult Search(int Id,string Search)
        //{
        //   // TempData["SearchResults"]
        //    var DashBoardModel = _assembler.AssembleSearch(Id, Search);
        //    //return Json(Url.Action("LoadModelAction", new { model = DashBoardModel }));
        //    //return RedirectToAction("LoadModelAction");
        //    return View("Dashboard", DashBoardModel);
        //}

        public ActionResult Search(int Id, string Search)
        {
            //Some logic to persist the change to DB.
            //var DashBoardModel
            ModelState.Clear();
            TempData["Search"] = Search;
            TempData["SearchResults"] = _assembler.AssembleSearch(Id, Search);
            var redirectUrl = new UrlHelper(Request.RequestContext).Action("LoadModelAction", "Dashboard");
            return Json(new { Url = redirectUrl });
        }

        public ActionResult Assign(int leadId,int salesRepId)
        {
            var modal = _assembler.AssembleAssignInfo(leadId, salesRepId);
            return PartialView("_AssignToUser", modal);
        }

        [HttpPost]
        public ActionResult Assign(RepInformation modal)
        {
            _contextAssembler.AssignLeadToUser(modal);
            var model = _assembler.AssembleToManage(modal.SalesRepId);
            return View("Manage", model);
        }

        
        public RedirectToRouteResult AssignToMe(int leadId, int salesRepId)
        {
            _contextAssembler.AssignLeadToUser(leadId, salesRepId);         
            TempData["SearchResults"] = _assembler.AssembleSearch(salesRepId, TempData["Search"].ToString());
            return RedirectToAction("LoadModelAction");
        }

    }
}