﻿using System.Web.Mvc;
using SunshareCommon;
using SunshareDatabase;
using SunshareHubOne.ContextAssembler;
using SunshareHubOne.ModelAssembler;
using SunshareHubOne.Models;
using System.Linq;
using System;

namespace SunshareHubOne.Controllers
{
    [Authorize]
    public class LeadController : Controller
    {
        private static readonly SunshareDatabaseContext Context = new SunshareDatabaseContext();
        readonly LeadViewModelAssembler _assembler = new LeadViewModelAssembler(Context);
        readonly DashboardViewModelAssembler _dashboardViewModelAssembler = new DashboardViewModelAssembler(Context);
        readonly LeadContextAssembler _contextAssembler = new LeadContextAssembler();
        // GET: Lead
        [HttpGet]
        public ActionResult Index(int Id)
        {
            ModelState.Clear();
            var model = _assembler.Assemble(Id);
            model.SalesRepId = Id;
            return View(model);
        }
        
        public ActionResult Edit(int salesRepId, int leadId)
        {
            ModelState.Clear();
            var model = _assembler.Assemble(salesRepId, leadId);
            model.SalesRepId = salesRepId;
            model.IsEdit = true;
            return View("Index", model);
        }

        [HttpPost]
        public ActionResult MultipleCommand(LeadViewModel model, string command)
        {
            var salesRepId = model.SalesRepId;
            var leadId = model.LeadId;
            ModelState.Clear();
            if (command== Messages.SubmitTypeClear)
                return RedirectToAction("Index","DashBoard", new {Id= salesRepId });
            if (!ModelState.IsValid)
            {
                //var errors = ModelState.Select(x => x.Value.Errors)
                //          .Where(y => y.Count > 0)
                //          .ToList();
                //foreach(var err in errors)
                //{
                //    foreach (var er in err)
                //        TempData["ErrorMessage"] = TempData["ErrorMessage"] + er.ErrorMessage;
                //}

                //ModelState.Clear();
                return RedirectToAction("Index","Lead", new { Id = salesRepId });
            }
            var methods = new Methods();
            bool result = false;
            switch (command)
            {
                case Messages.SubmitTypeSave:
                    result = _contextAssembler.Assemble(model, ContractOptions.SaveLead);
                    if (result)
                        TempData[Messages.SuccessTempdata] = Messages.LeadCreatedSuccessfully;
                    else
                        TempData[Messages.ErrorTempData] = Messages.LeadWithEmailExisits;
                    break;
                // context.SaveChanges();
                case Messages.SubmitTypeInfo:
                    result = _contextAssembler.Assemble(model, ContractOptions.SendInfo);
                    if (result)
                        TempData[Messages.SuccessTempdata] = Messages.InfoSentSuccessfully;
                    else
                        TempData[Messages.ErrorTempData] = Messages.LeadWithEmailExisits;
                    break;
                case Messages.SubmitTypeContract:
                    result = _contextAssembler.Assemble(model, ContractOptions.GenerateContract);
                    if (result)
                        TempData[Messages.SuccessTempdata] = Messages.ContractGeneratedSuccessfully;
                    else
                        TempData[Messages.ErrorTempData] = Messages.LeadWithEmailExisits;
                    break;                
            }
            if (!result)
            {
               // ModelState.Clear();
                if (leadId == 0)
                    return RedirectToAction("Index","Lead", new { Id = salesRepId });
                else
                    return RedirectToAction("Edit","Lead", new { salesRepId = salesRepId, leadId = leadId });
            }
            
            //ModelState.Clear();
            return RedirectToAction("Index","Dashboard", new { Id = salesRepId });
        }

        public ActionResult Refresh(int leadid, int repId)
        {
            _contextAssembler.GetLeadContractStatus(leadid);
            return Json(Url.Action("Edit", new { salesRepId = repId, leadId = leadid }));
        }
        [HttpPost]
        public ActionResult CallLog(string callComment,int leadId,int callStatusId,string callDate,int repId)
        {
            DateTime? Date = string.IsNullOrEmpty(callDate) ? (DateTime?)null : Convert.ToDateTime(callDate);
            _contextAssembler.LogCall(callComment, leadId, callStatusId, Date);
            return Json(Url.Action("Edit", new { salesRepId = repId, leadId = leadId }));
        }
    }
}