﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SunshareHubOne.Models
{
    public class DashboardViewModel
    {
        public DashboardViewModel()
        {
            Leads=new List<LeadInformation>();
            //SearchLeads = new List<LeadInformation>();
        }
        public ICollection<LeadInformation> Leads { get; set; }

        public ICollection<LeadInformation> SearchLeads { get; set; }

        public int ContractSent { get; set; }

        public int ContractSigned { get; set; }

        public int ContractDeclined { get; set; }

        public int InformationSent { get; set; }

        public int ActionPending { get; set; }

        public int Appointment { get; set; }

        public int SalesRepId { get; set; }
    }

    public class LeadInformation
    {
        public int LeadId { get; set; }
        public int SalesRepId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }

        public string State { get; set; }

        public string Email { get; set; }
        public string DateTime { get; set; }

        public int LeadStausId { get; set; }

        public int CallCount { get; set; }

        public int AppointmnetCount { get; set; }
    }

    public class RepInformation
    {
        public int SalesRepId { get; set; }
        public int LeadId { get; set;}
        [DisplayName("Representative")]
        public List<SelectListItem> Reps { get; set; }
        public int SelectedRepId { get; set; }
        
    }
    public class Rep
    {
        public int RepId { get; set; }

        public int RepName { get; set; }
    }
}
