﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SunshareHubOne.Models
{
    public class LeadViewModel
    {
        [Required(ErrorMessage = "Please enter first name")]
        [RegularExpression(@"([a-zA-Z]{1}[a-zA-Z]*[\s]{0,1}[a-zA-Z])+([\s]{0,1}[a-zA-Z]+)", ErrorMessage ="Enter proper names only")]
        [DisplayName("First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter last name")]
        [RegularExpression(@"([a-zA-Z]{1}[a-zA-Z]*[\s]{0,1}[a-zA-Z])+([\s]{0,1}[a-zA-Z]+)", ErrorMessage = "Enter proper names only")]
        [DisplayName("Last name")]
        public string LastName { get; set; }

        [DisplayName("Apartment / Door No")]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Apartment / Door No must be a number.")]
        public string ServiceApartmentNo { get;set; }

        [DisplayName("Apartment / Door No")]
        public string BillingApartmentNo { get; set; }

        [DisplayName("Address")]
        [Required(ErrorMessage = "Please enter service address")]
        public string ServiceAddress { get; set; }

        [DisplayName("Address")]
        public string BillingAddress { get; set; }

        public string Phone { get; set; }

        [DisplayName("State")]
        public IQueryable<SelectListItem> States { get; set; }

        [DisplayName("State")]
        [Required(ErrorMessage ="Please select a state")]
        public int SerStateId { get; set; }

        [DisplayName("State")]
        public int BilStateId { get; set; }

        [DisplayName("City")]
        [Required(ErrorMessage ="Please select a city")]
        public string ServiceCity { get; set; }

        [DisplayName("City")]
        public string BillingCity { get; set; }

        [DisplayName("County")]
        public IQueryable<SelectListItem> Counties { get; set; }

        [DisplayName("County")]
        [Required(ErrorMessage ="Please enter city")]
        public int SerCountyId { get; set; }

        [DisplayName("County")]
        public int BilCountyId { get; set; }

        [DisplayName("Zip Code")]
        [Required(ErrorMessage ="Please enter a zip code")]
        [RegularExpression(@"(^(?!0{5})(\d{5})(?!-?0{4})(|-\d{4})?$)", ErrorMessage = "Please enter valid ZIP or ZIP+4 code")]
        public string SerZipCode { get; set; }

        [DisplayName("Zip Code")]
        [RegularExpression(@"(^(?!0{5})(\d{5})(?!-?0{4})(|-\d{4})?$)", ErrorMessage = "Please enter valid ZIP or ZIP+4 code")]
        public string BilZipCode { get; set; }

        [DisplayName("Contract Status")]
        public int LeadStausId { get; set; }

        public string LeadStatus { get; set; }
        [Required(ErrorMessage = "The email address is required")]
        [RegularExpression(@"^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-zA-Z0-9]{1}[a-zA-Z0-9\-]{0,62}[a-zA-Z0-9]{1})|[a-zA-Z])\.)+[a-zA-Z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$",ErrorMessage ="Please enter a valid Email Address")]
        public string Email { get; set; }

        public IQueryable<SelectListItem> Subscriptions { get; set; }

        public string Subscription { get; set; }

        [DisplayName("Utility Account Number")]
        //[Required(ErrorMessage = "Please enter utility account number")]
        //[RegularExpression(@"^[0-9]+(-[0-9]+)+$",ErrorMessage = "UAN must be similer to xxx-xxxx-xxxx format")]
        public string UtilityAccountNumber { get; set; }

        [DisplayName("Utility Provider")]
        public string UtilityProvider { get; set; }

        [DisplayName("Sales Channel")]
        public IQueryable<SelectListItem> Channels { get; set; }

        [DisplayName("Sales Channel")]
        public int ChannelId { get; set; }

        [DisplayName("Campaign")]
        public IQueryable<SelectListItem> Campaigns { get; set; }

        [DisplayName("Campaign")]
        public int CampaignId { get; set; }

        [DisplayName("Referred By")]
        [RegularExpression(@"([a-zA-Z]{1}[a-zA-Z]*[\s]{0,1}[a-zA-Z])+([\s]{0,1}[a-zA-Z]+)", ErrorMessage = "Enter proper names only")]
        public string ReferedBy { get; set; }

        [DisplayName("Incentive")]
        public string Incentive { get; set; }

        [DisplayName("Lead Source")]
        public IQueryable<SelectListItem> LeadSources { get; set; }

        [DisplayName("Lead Source")]
        public int LeadSourceId { get; set; }

        public int SalesRepId { get; set; }

        [DisplayName("Date Of Birth")]
        public DateTime? DateOfBirth { get; set; }

        [DisplayName("Lead Ranking")]
        public IQueryable<SelectListItem> LeadRankings { get; set; } 

        public int LeadRankingId { get; set; }

        public int LeadId { get; set; }

        public bool IsEdit { get; set; }

        [DisplayName("Comments")]
        public string CallComment { get; set; }

        public bool? IsDuplicateCall { get; set; }

        public int CallStatusId { get; set; }

        [DisplayName("Call Status")]
        public IQueryable<SelectListItem> CallStatus { get; set; }

        [DisplayName("Appointment Date")]
        public DateTime? CallDate { get; set; }

        public int CallLogId { get; set; }

        public ICollection<LeadHistory> LeadHistories { get; set; }
        
    }

    public class LeadHistory
    {
        public string Title { get; set; }

        public string Date { get; set; }

        public string ProcessDate { get; set; }

        public string Comments { get; set; }

        public string DoneBy { get; set; }
    }
}
