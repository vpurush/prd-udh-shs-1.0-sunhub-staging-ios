﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SunshareDatabase;
using SunshareHubOne.Models;
using SunshareCommon;
using System.Web.Mvc;
using System.Data.Entity;

namespace SunshareHubOne.ModelAssembler
{
    public interface IDashBoardViewModelAssember
    {
        DashboardViewModel Assemble(int salesRepId,bool admin);
    }
    public class DashboardViewModelAssembler : IDashBoardViewModelAssember
    {
        protected SunshareDatabaseContext Sunshare { get; set; }

        public DashboardViewModelAssembler(SunshareDatabaseContext context)
        {
            Sunshare = context;
        }
        public DashboardViewModel Assemble(int salesRepId,bool admin=false)
        {
            var model=new DashboardViewModel();
            model.SalesRepId = salesRepId;
            var myLeads = (!admin)?Sunshare.Leads.Where(l => l.IsActive & l.AspNetUsersId == salesRepId).ToList(): Sunshare.Leads.Where(l => l.IsActive).ToList();            
            foreach (var lead in myLeads)
            {
                var leadAddress =
                    Sunshare.Address.FirstOrDefault(la => la.IsActive & la.AddressId == lead.ServiceAddressId);
                var leadLeadStatusMapping = Sunshare.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent & l.LeadId == lead.LeadId);
                model.Leads.Add(new LeadInformation 

                {
                    LeadId = lead.LeadId,
                    SalesRepId = lead.AspNetUsersId,
                    Name = $"{lead.FirstName} {lead.LastName}",
                    Email = lead.PrimaryEmail,
                    Phone = lead.HomePhoneNo,
                    DateTime = lead.DateCreated?.ToString("yyyy-MM-dd H:mm:ss") ?? "N/A", //TODO: needs to be changed.
                    State = leadAddress?.State,
                    LeadStausId = leadLeadStatusMapping?.LeadStatusId ?? 0,
                    CallCount=Sunshare.CallLogs.Count(c=>c.LeadId==lead.LeadId),
                    AppointmnetCount = Sunshare.Leads.Count(
                     l =>
                         Sunshare.CallLogs.Where(c => c.LeadId == l.LeadId)
                             .Any(x => Sunshare.CallAppointment.Any(a => a.CallLogId == x.CallLogId & DbFunctions.DiffDays(a.CallDate, DateTime.Now) <= 0)))
            }
                    );
            }

            model.ContractDeclined = myLeads.Count(l => Sunshare.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadId == l.LeadId & s.LeadStatusId==LeadContractStatus.ContractDeclined));
            model.ContractSigned = myLeads.Count(l => Sunshare.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadId == l.LeadId & s.LeadStatusId == LeadContractStatus.ContractSigned));
            model.ContractSent = myLeads.Count(l => Sunshare.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadId == l.LeadId & s.LeadStatusId == LeadContractStatus.ContractSent));
            model.InformationSent = myLeads.Count(l => Sunshare.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadId == l.LeadId & s.LeadStatusId == LeadContractStatus.CotractInformationSent));
            model.ActionPending =
                myLeads.Count(
                    l =>
                        l.IsActive &
                        Sunshare.LeadLeadStatusMappings.Any(
                            s => s.IsCurrent & s.LeadId == l.LeadId & s.LeadStatusId == LeadContractStatus.LeadCreated) &
                        !Sunshare.CallLogs.Any(c => c.LeadId == l.LeadId));
            model.Appointment =
                 myLeads.Count(
                     l =>
                         Sunshare.CallLogs.Where(c => c.LeadId == l.LeadId)
                             .Any(x => Sunshare.CallAppointment.Any(a => a.CallLogId == x.CallLogId & DbFunctions.DiffDays(a.CallDate,DateTime.Now) <= 0)));
            // throw new NotImplementedException();
            return model;
        }

        public RepInformation AssembleAssignInfo(int leadId,int salesRepId)
        {
            var repInfo = new RepInformation()
            {
                LeadId = leadId,
                SalesRepId = salesRepId,
                Reps = Sunshare.Users.Where(u => u.UserParentId == salesRepId).Select(s => new SelectListItem() { Text = s.FirstName, Value = s.Id.ToString() }).ToList()
            };
            return repInfo;
        }

        public DashboardViewModel AssembleSearch(int salesRepId,string searchText)
        {
            var model = new DashboardViewModel();
            model.SalesRepId = salesRepId;
            var myLeads = Sunshare.Leads.Where(l => l.IsActive & l.AspNetUsersId == salesRepId).ToList();
            foreach (var lead in myLeads)
            {
                var leadAddress =
                    Sunshare.Address.FirstOrDefault(la => la.IsActive & la.AddressId == lead.ServiceAddressId);
                var leadLeadStatusMapping = Sunshare.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent & l.LeadId == lead.LeadId);
                model.Leads.Add(new LeadInformation

                {
                    LeadId = lead.LeadId,
                    SalesRepId = lead.AspNetUsersId,
                    Name = $"{lead.FirstName} {lead.LastName}",
                    Email = lead.PrimaryEmail,
                    Phone = lead.HomePhoneNo,
                    DateTime = lead.DateCreated?.ToString("yyyy-MM-dd H:mm:ss") ?? "N/A", //TODO: needs to be changed.
                    State = leadAddress?.State,
                    LeadStausId = leadLeadStatusMapping?.LeadStatusId ?? 0,
                    CallCount = Sunshare.CallLogs.Count(c => c.LeadId == lead.LeadId)
                }
                    );
            }

            //TODO: Ugly Coding. Entire control is coded in a very bad way- Andrew. And I'm responsible.
            //searchText = "subashandrew@gmail.com";
            var searchLeads = Sunshare.Leads.Where(l => (l.IsActive & l.PrimaryEmail==searchText || l.HomePhoneNo == searchText) & l.AspNetUsersId != salesRepId).ToList();
            model.SearchLeads = new List<LeadInformation>();
            foreach (var lead in searchLeads)
            {
                var leadAddress =
                    Sunshare.Address.FirstOrDefault(la => la.IsActive & la.AddressId == lead.ServiceAddressId);
                var leadLeadStatusMapping = Sunshare.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent & l.LeadId == lead.LeadId);
                model.SearchLeads.Add(new LeadInformation

                {
                    LeadId = lead.LeadId,
                    SalesRepId = lead.AspNetUsersId,
                    Name = $"{lead.FirstName} {lead.LastName}",
                    Email = lead.PrimaryEmail,
                    Phone = lead.HomePhoneNo,
                    DateTime = lead.DateCreated?.ToString("yyyy-MM-dd H:mm:ss") ?? "N/A", //TODO: needs to be changed.
                    State = leadAddress?.State,
                    LeadStausId = leadLeadStatusMapping?.LeadStatusId ?? 0,
                    CallCount = Sunshare.CallLogs.Count(c => c.LeadId == lead.LeadId)
                }
                    );
            }

            model.ContractDeclined = myLeads.Count(l => Sunshare.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadId == l.LeadId & s.LeadStatusId == LeadContractStatus.ContractDeclined));
            model.ContractSigned = myLeads.Count(l => Sunshare.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadId == l.LeadId & s.LeadStatusId == LeadContractStatus.ContractSigned));
            model.ContractSent = myLeads.Count(l => Sunshare.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadId == l.LeadId & s.LeadStatusId == LeadContractStatus.ContractSent));
            model.InformationSent = myLeads.Count(l => Sunshare.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadId == l.LeadId & s.LeadStatusId == LeadContractStatus.CotractInformationSent));
            model.ActionPending =
                myLeads.Count(
                    l =>
                        l.IsActive &
                        Sunshare.LeadLeadStatusMappings.Any(
                            s => s.IsCurrent & s.LeadId == l.LeadId & s.LeadStatusId == LeadContractStatus.LeadCreated) &
                        !Sunshare.CallLogs.Any(c => c.LeadId == l.LeadId));
            model.Appointment =
                 myLeads.Count(
                     l =>
                         Sunshare.CallLogs.Where(c => c.LeadId == l.LeadId)
                             .Any(x => Sunshare.CallAppointment.Any(a => a.CallLogId == x.CallLogId & DbFunctions.DiffDays(a.CallDate, DateTime.Now) <= 0)));
            // throw new NotImplementedException();
            return model;
        }

        public DashboardViewModel AssembleToManage(int salesRepId)
        {
            var model = new DashboardViewModel();
            model.SalesRepId = salesRepId;
            var myLeads = Sunshare.Leads.Where(l => l.IsActive & l.AspNetUsersId == 0).ToList();
            foreach (var lead in myLeads)
            {
                var leadAddress =
                    Sunshare.Address.FirstOrDefault(la => la.IsActive & la.AddressId == lead.ServiceAddressId);
                var leadLeadStatusMapping = Sunshare.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent & l.LeadId == lead.LeadId);
                model.Leads.Add(new LeadInformation

                {
                    LeadId = lead.LeadId,
                    SalesRepId = lead.AspNetUsersId,
                    Name = $"{lead.FirstName} {lead.LastName}",
                    Email = lead.PrimaryEmail,
                    Phone = lead.HomePhoneNo,
                    DateTime = lead.DateCreated?.ToString("yyyy-MM-dd H:mm:ss") ?? "N/A", //TODO: needs to be changed.
                    State = leadAddress?.State,
                    LeadStausId = leadLeadStatusMapping?.LeadStatusId ?? 0,
                    CallCount = Sunshare.CallLogs.Count(c => c.LeadId == lead.LeadId)
                }
                    );
            }

            //model.ContractDeclined = myLeads.Count(l => Sunshare.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadId == l.LeadId & s.LeadStatusId == LeadContractStatus.ContractDeclined));
            //model.ContractSigned = myLeads.Count(l => Sunshare.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadId == l.LeadId & s.LeadStatusId == LeadContractStatus.ContractSigned));
            //model.ContractSent = myLeads.Count(l => Sunshare.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadId == l.LeadId & s.LeadStatusId == LeadContractStatus.ContractSent));
            //model.InformationSent = myLeads.Count(l => Sunshare.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadId == l.LeadId & s.LeadStatusId == LeadContractStatus.CotractInformationSent));
            // throw new NotImplementedException();
            return model;
        }
    }
}
