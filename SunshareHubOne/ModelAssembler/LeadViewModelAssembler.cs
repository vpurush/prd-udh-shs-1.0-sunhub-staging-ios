﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using SunshareCommon;
using SunshareDatabase;
using SunshareHubOne.Models;

namespace SunshareHubOne.ModelAssembler
{
    public interface ILeadViewModelAssembler
    {
        LeadViewModel Assemble(int salesRepId);
        LeadViewModel Assemble(int salesRepId, int leadId);
    }
    public class LeadViewModelAssembler : ILeadViewModelAssembler
    {
        protected SunshareDatabaseContext Sunshare { get; set; }
        public LeadViewModelAssembler(SunshareDatabaseContext context)
        {
            Sunshare = context;
        }
        public LeadViewModel Assemble(int salesRepId)
        {            
            var model = new LeadViewModel
            {
                States = Sunshare.States.Where(s => s.IsActive)
                    .Select(s => new SelectListItem() { Text = s.StateName, Value = s.StateId.ToString() }),
                Counties = Sunshare.Counties.Where(c => c.IsActive).Select(c => new SelectListItem() { Text = c.CountyName, Value = c.CountyId.ToString() }),
                Channels=Sunshare.Channels.Where(c => c.IsActive).Select(c => new SelectListItem() { Text = c.ChannelName, Value = c.ChannelId.ToString() }),
                Campaigns = Sunshare.Campaigns.Where(c => c.IsActive).Select(c => new SelectListItem() { Text = c.CampaignName, Value = c.CampaignId.ToString() }),
                LeadRankings = Sunshare.LeadRakings.Select(r => new SelectListItem() { Text = r.RankingName, Value = r.LeadRankingId.ToString() }),
                SalesRepId = salesRepId,
                LeadSources = Sunshare.LeadSources.Where(l => l.IsActive).Select(l => new SelectListItem() { Text = l.LeadSourceName, Value = l.LeadSourceId.ToString() }),
                CallStatus = Sunshare.CallStatus.Select(c => new SelectListItem() { Text = c.CallStatusName, Value = c.CallStatusId.ToString() }),
                LeadStausId = 0,
                Subscription="100",
                LeadHistories=new List<LeadHistory>()
            };
            return model;
            //throw new NotImplementedException();
        }

        

        public LeadViewModel Assemble(int salesRepId, int leadId)
        {
            LeadViewModel model = null;
            var lead = Sunshare.Leads.FirstOrDefault(l => l.LeadId == leadId);
            var address = Sunshare.Address.FirstOrDefault(a => a.AddressId == lead.ServiceAddressId);
            if (lead == null) return new LeadViewModel();
            if (address == null) return new LeadViewModel();
            {
                model = new LeadViewModel
                {
                    States = Sunshare.States.Where(s => s.IsActive)
                        .Select(s => new SelectListItem() { Text = s.StateName, Value = s.StateId.ToString() }),
                    Counties = Sunshare.Counties.Where(c => c.IsActive).Select(c => new SelectListItem() { Text = c.CountyName, Value = c.CountyId.ToString() }),
                    Channels = Sunshare.Channels.Where(c => c.IsActive).Select(c => new SelectListItem() { Text = c.ChannelName, Value = c.ChannelId.ToString() }),
                    Campaigns = Sunshare.Campaigns.Where(c => c.IsActive).Select(c => new SelectListItem() { Text = c.CampaignName, Value = c.CampaignId.ToString() }),
                    LeadRankings = Sunshare.LeadRakings.Select(r => new SelectListItem() { Text = r.RankingName, Value = r.LeadRankingId.ToString() }),
                    LeadSources = Sunshare.LeadSources.Where(l => l.IsActive).Select(l => new SelectListItem() { Text = l.LeadSourceName, Value = l.LeadSourceId.ToString() }),
                    CallStatus = Sunshare.CallStatus.Select(c => new SelectListItem() { Text = c.CallStatusName, Value = c.CallStatusId.ToString() }),
                    SalesRepId = leadId,
                    FirstName = lead.FirstName,
                    LastName = lead.LastName,
                    ServiceApartmentNo=address.AppartmentNo,
                    ServiceAddress = address.AddressInformation,
                    SerZipCode = address.Zipcode,
                    ServiceCity = address.City,
                    Email = lead.PrimaryEmail,
                    Phone = lead.HomePhoneNo,
                    LeadRankingId = lead.LeadRankingId,
                    LeadId = lead.LeadId,
                    ReferedBy = lead.RefferedBy,
                    CampaignId = lead.CampaignId,
                    LeadSourceId = lead.LeadSourceId ?? 0,
                    UtilityAccountNumber = lead.UtilityAccountNo,
                    Subscription = lead.SubscriptionPercentage.ToString(),
                    DateOfBirth = Convert.ToDateTime(lead.DateOfBirth)
                    };
                var leadStatus =
                    Sunshare.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent & l.LeadId == leadId);
                if(leadStatus!=null)
                model.LeadStausId = leadStatus.LeadStatusId;
                var stateInfo = Sunshare.States.FirstOrDefault(s => s.StateName == address.State);
                if (stateInfo != null)
                    model.SerStateId = stateInfo.StateId;
                var countyInfo = Sunshare.Counties.FirstOrDefault(c => c.CountyName == address.County);
                if (countyInfo != null)
                    model.SerCountyId = countyInfo.CountyId;
                
                model.LeadHistories=GetLeadHistory(leadId);
            }
            return model;
            //throw new NotImplementedException();
        }

        private ICollection<LeadHistory> GetLeadHistory(int leadId)
        {
            var hisotories = new List<LeadHistory>();
            var statusHistory=Sunshare.LeadLeadStatusMappings.Where(l => l.LeadId == leadId);
            var callHistory = Sunshare.CallLogs.Where(l => l.LeadId == leadId);
            var assignHistory = Sunshare.AssignmentHistory.Where(l => l.LeadId == leadId);
            foreach(var status in statusHistory)
            {
                var firstOrDefaultStatus = Sunshare.Status.FirstOrDefault(s => s.LeadStatusId == status.LeadStatusId);
                if (firstOrDefaultStatus != null)
                    hisotories.Add(new LeadHistory() {
                        Title = firstOrDefaultStatus.StatusName,
                        Date = status.DateCreated?.ToString("MM-dd-yyyy hh:mm") ?? string.Empty,
                        DoneBy = status.UserCreated,
                        //ProcessDate ="Last Update:"+ status.UpdatedDate.ToString("MM-dd-yyyy")                    
                    });
            }
            foreach(var call in callHistory)
            {
                var firstOrDefaultCallStatus = Sunshare.CallStatus.FirstOrDefault(s => s.CallStatusId == call.CallStatusId);
                if (firstOrDefaultCallStatus == null) continue;
                var appointment = Sunshare.CallAppointment.FirstOrDefault(c => c.CallLogId == call.CallLogId);
                hisotories.Add(new LeadHistory()
                {
                    Title = "Call :" + firstOrDefaultCallStatus.CallStatusName,
                    Date = call.DateCreated?.ToString("MM-dd-yyyy hh:mm") ?? string.Empty,
                    DoneBy = call.UserCreated,
                    Comments = call.CallComment,
                    ProcessDate ="Appointment Date:"+((appointment != null)?Convert.ToString(appointment.CallDate, CultureInfo.CurrentCulture):string.Empty),
                });
            }
            foreach (var assignment in assignHistory)
            {
                var firstOrDefaultUser = Sunshare.Users.FirstOrDefault(s => s.Id == assignment.AspNetUserId);
                if (firstOrDefaultUser != null)
                    hisotories.Add(new LeadHistory()
                    {
                        Title = "Assigned To :" + firstOrDefaultUser.UserName,
                        Date = assignment.DateCreated?.ToString("MM-dd-yyyy hh:mm") ?? string.Empty,
                        DoneBy = assignment.UserCreated,
                        //Comments = assignment.,
                        //ProcessDate = Convert.ToString(call.Appointments.FirstOrDefault(c => c.CallLogId == call.CallLogId).CallDate)
                    });
            }
            return hisotories;
        }

        internal void Assemble(int leadId, string command)
        {
            throw new NotImplementedException();
        }
    }
}
