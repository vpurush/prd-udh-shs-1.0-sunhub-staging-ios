﻿var toastTypes =
{
    message: 1,
    success: 2,
    warning: 3,
    error: 4
};

(function ($) {
    var methods =
    {
        addToast: function (toastData) {
            return this.each(function () {
                var $toastHost = $(this);
                methods._addToast($toastHost, toastData);
            });
        },

        toastConfirm: function (message, buttons, resultCallback) {
            var $toastHost = $(this);

            // remove existing confirms if any
            $toastHost.children("DIV.confirm").remove();

            var $toast = $("<div><div class=\"question\">" + message + "</div></div>");
            $toast.addClass("toast confirm");

            var $buttonHost = $("<div class=\"buttons\"></div>");
            $toast.append($buttonHost);

            for (var i = 0; i < buttons.length; i++) {
                var $button = $("<div class=\"button\"></div>");
                $button.text(buttons[i]);
                $button.click(function () {
                    var $this = $(this);
                    $toast.remove();
                    methods._showHideContainer($toastHost);
                    if (resultCallback) resultCallback($this.text());
                });

                $buttonHost.append($button);
            }

            $toastHost.append($toast);
            methods._showHideContainer($toastHost);
            $toast.hide();
            $toast.slideDown();
        },

        init: function () {
            return this.each(function () {
                var $this = $(this);

                if (methods._showHideContainer($this)) {
                    $this.children("DIV.toast").each(function (index, toast) {
                        methods._setupToast($(toast));
                    });
                }
            });
        },

        _showHideContainer: function ($toastHost) {
            if ($toastHost.children().size() < 1) {
                $toastHost.hide();
                return false;
            }

            $toastHost.show();
            return true;
        },

        _removeToast: function ($toast) {
            var $toastHost = $toast.closest("DIV.toast-host");

            $toast.hide(500, function () {
                $toast.remove();
                methods._showHideContainer($toastHost);
            });
        },

        _addToast: function ($toastHost, toastData) {
            var element = $("<div>" + toastData.message + "</div>");
            element.addClass("toast");

            if (toastData.type === toastTypes.message) element.addClass("message");
            if (toastData.type === toastTypes.success) element.addClass("success");
            if (toastData.type === toastTypes.warning) element.addClass("warning");
            if (toastData.type === toastTypes.error) element.addClass("error");

            if (toastData.sticky === true) {
                element.addClass("sticky");
                element.prepend("<div class=\"close\">x</div>");
            }

            $toastHost.append(element);
            methods._showHideContainer($toastHost);
            methods._setupToast(element);
        },

        _setupToast: function ($toast) {
            if ($toast.hasClass("sticky")) {
                $toast.children("DIV.close").click(
                    $.proxy(function () {
                        methods._removeToast($(this));
                    }, $toast));
            }
            else {
                setTimeout(function () {
                    methods._removeToast($toast);
                }, 3000);
            }
        }
    };

    // Extend jQuery with the function
    $.fn.toast = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        }
        else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        }

        $.error('Method ' + method + ' does not exist on jQuery.toast');
        return null;
    };

    // note, this is not best practice to add another JQuery fn, but makes sense in this case
    $.fn.addToast = function () {
        return methods.addToast.apply(this, arguments);
    };

    $.fn.toastConfirm = function () {
        return methods.toastConfirm.apply(this, arguments);
    };

})(jQuery);

