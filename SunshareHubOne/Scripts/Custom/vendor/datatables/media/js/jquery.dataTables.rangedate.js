﻿/**
 * Filter a column on a specific date range. Note that you will likely need 
 * to change the id's on the inputs and the columns in which the start and 
 * end date exist.
 *
 *  @name Date range filter
 *  @summary Filter the table based on two dates in different columns
 *  @author _guillimon_
 *
 *  @example
 *    $(document).ready(function() {
 *        var table = $('#example').DataTable();
 *         
 *        // Add event listeners to the two range filtering inputs
 *        $('#min').keyup( function() { table.draw(); } );
 *        $('#max').keyup( function() { table.draw(); } );
 *    } );
 */


$.fn.dataTableExt.afnFiltering.push(
    function(oSettings, aData, iDataIndex) {
        // "date-range" is the id for my input


        // parse the range from a single field into min and max, remove " - "

        var dateMin = document.getElementById('min').value;
        var dateMax = document.getElementById('max').value;
        // 4 here is the column where my dates are.
        var date = aData[0];

        // remove the time stamp out of my date
        // 2010-04-11 20:48:22 -> 2010-04-11
        date = date.substring(0, 10);
        // remove the "-" characters
        // 2010-04-11 -> 20100411
        date = date.substring(0, 4) + date.substring(5, 7) + date.substring(8, 10);

        alert(date);
        if (dateMin === "" && dateMax === "") {
            return true;
        } else if (dateMin <= date && dateMax === "") {
            return true;
        } else if (dateMax >= date && dateMin === "") {
            return true;
        } else if (dateMin <= date && dateMax >= date) {
            return true;
        }
        return false;
    });

//$.fn.dataTableExt.afnFiltering.push(
//	function (oSettings, aData, iDataIndex) {

//	    var iFini = document.getElementById('min').value;
//	    var iFfin = document.getElementById('max').value;
//	    var iStartDateCol = 6;
//	    var iEndDateCol = 7;

//	    iFini = iFini.substring(6, 10) + iFini.substring(3, 5) + iFini.substring(0, 2);
//	    iFfin = iFfin.substring(6, 10) + iFfin.substring(3, 5) + iFfin.substring(0, 2);

//	    var datofini = aData[iStartDateCol].substring(6, 10) + aData[iStartDateCol].substring(3, 5) + aData[iStartDateCol].substring(0, 2);
//	    var datoffin = aData[iEndDateCol].substring(6, 10) + aData[iEndDateCol].substring(3, 5) + aData[iEndDateCol].substring(0, 2);

//	    if (iFini === "" && iFfin === "") {
//	        return true;
//	    }
//	    else if (iFini <= datofini && iFfin === "") {
//	        return true;
//	    }
//	    else if (iFfin >= datoffin && iFini === "") {
//	        return true;
//	    }
//	    else if (iFini <= datofini && iFfin >= datoffin) {
//	        return true;
//	    }
//	    return false;
//	}
//);
