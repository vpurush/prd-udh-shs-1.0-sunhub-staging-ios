﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;


namespace SunshareHubOne.Extensions
{
    public static class HtmlHelperExtensions
    {
        private static HttpContextWrapper Wrapper { get; set; } = new HttpContextWrapper(HttpContext.Current);

        public static IHtmlString TextToHtmlDisplay(this HtmlHelper htmlHelper, string textToFormat)
        {
            return htmlHelper.Raw(htmlHelper.Encode(textToFormat).Replace("\n", "<br/>"));
        }

        public static HtmlString Stylesheets(this HtmlHelper htmlHelper, string contentDirectory)
        {
            // Do we have a cached version?
            var cacheKey = "styles_in_" + contentDirectory;
            var cache = GetAppCache(cacheKey);
            if (!string.IsNullOrEmpty(cache)) return new HtmlString(cache);

            // Get the stylesheets in the directory and compose html includes
            var files = GetFilesIn(
                htmlHelper.ViewContext.HttpContext.Server,
                Path.Combine("~/Scripts/styles", contentDirectory),
                "*.css");

            var output = new StringBuilder();
            foreach (var filename in files)
            {
                var file = Path.Combine(contentDirectory, filename);
                output.Append(BuildStylesheet(htmlHelper, file));
            }

            // Cache it for default timespan
            cache = output.ToString();
            SetAppCache(cacheKey, cache);
            return new HtmlString(cache);
        }

        public static HtmlString Stylesheet(this HtmlHelper htmlHelper, params string[] stylesheets)
        {
            return new HtmlString(BuildStylesheet(htmlHelper, stylesheets));
        }

        private static string BuildStylesheet(HtmlHelper htmlHelper, params string[] stylesheets)
        {
            var output = new StringBuilder();
            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            foreach (var stylesheet in stylesheets)
            {
                var urlPath = Path.Combine("~/Scripts/styles/", stylesheet);
                var url = urlHelper.Content(urlPath);
                output.AppendFormat("\n\t<link href=\"{0}\" rel=\"Stylesheet\" type=\"text/css\"/>", url);
            }

            return output.ToString();
        }

        public static HtmlString Scripts(this HtmlHelper htmlHelper, string scriptDirectory)
        {
            // Do we have a cached version?
            var cacheKey = "scripts_in_" + scriptDirectory;
            var cache = GetAppCache(cacheKey);
            if (!string.IsNullOrEmpty(cache)) return new HtmlString(cache);

            // Get the scripts in the directory and compose html includes
            var files = GetFilesIn(
                htmlHelper.ViewContext.HttpContext.Server,
                Path.Combine("~/Scripts/", scriptDirectory),
                "*.js");

            var output = new StringBuilder();
            foreach (var filename in files)
            {
                var file = Path.Combine(scriptDirectory, filename);
                output.Append(BuildScript(htmlHelper, file));
            }

            // Cache it for default timespan
            cache = output.ToString();
            SetAppCache(cacheKey, cache);
            return new HtmlString(cache);
        }

        public static HtmlString Script(this HtmlHelper htmlHelper, params string[] scripts)
        {
            return new HtmlString(BuildScript(htmlHelper, scripts));
        }

        private static string BuildScript(HtmlHelper htmlHelper, params string[] scripts)
        {
            var output = new StringBuilder();
            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            foreach (var script in scripts)
            {
                var urlPath = Path.Combine("~/Scripts/", script);
                var url = urlHelper.Content(urlPath);
                output.AppendFormat("\n\t<script src=\"{0}\" type=\"text/javascript\"></script>", url);
            }

            return output.ToString();
        }

        private static IEnumerable<string> GetFilesIn(HttpServerUtilityBase server, string basePath, string mask)
        {
            var physicalPath = server.MapPath(basePath);

            return Directory.GetFiles(physicalPath, mask)
                .Select(Path.GetFileName)
                .Where(file => !string.IsNullOrEmpty(file))
                .ToList();
        }

        private static string GetAppCache(string key)
        {
            
            var cache =new ApplicationCache(Wrapper);
            var cacheValue = cache.Get<string>(key);
            //ServiceLocator.Release(cache);

            return cacheValue;
        }

        private static void SetAppCache(string key, string value)
        {
            var cache = new ApplicationCache(Wrapper);
            cache.Add(key, value);
            //ServiceLocator.Release(cache);
        }
    }
}
