﻿using System;
using System.Globalization;
using System.Linq;
using SunshareCommon;
using SunshareDatabase;
using SunshareDatabaseD2DLiveCopy;
using SunshareHubOne.Models;
using Address = SunshareDatabase.Address;
using AddressUserMapping = SunshareDatabase.AddressUserMapping;
using CallAppointment = SunshareDatabase.CallAppointment;
using CallLog = SunshareDatabase.CallLog;
using Lead = SunshareDatabase.Lead;
using LeadLeadStatusMapping = SunshareDatabase.LeadLeadStatusMapping;
using LeadUserMappingHistory = SunshareDatabase.LeadUserMappingHistory;

namespace SunshareHubOne.ContextAssembler
{
    public interface ILeadContextAssmbler
    {
        bool Assemble(LeadViewModel model, int options);
    }
    public class LeadContextAssembler : ILeadContextAssmbler
    {
        private SunshareContext context;
        readonly Methods _methods = new Methods();
        public LeadContextAssembler()
        {
            context = new SunshareContext();
        }

        public bool Assemble(LeadViewModel model, int options)
        {
            SunshareDatabaseD2DLiveCopy.Lead lead = null;
            SunshareDatabaseD2DLiveCopy.Address address;
            if (model.LeadId == 0)
            {
                if (!context.Lead.Any(l => l.PrimaryEmail == model.Email || l.SecondaryEmail == model.Email))
                {
                    address = SaveLeadAddress(model);
                    SaveLeadAddressMapping(address, model.SalesRepId);
                    lead = SaveLead(model, address);
                    SaveLeadLeadStatusMapping(lead, LeadContractStatus.LeadCreated);
                    SaveLeadAssigningHistory(lead.LeadId, model.SalesRepId);
                }
                else
                    return false;
            }
            else
            {
                if (!context.Lead.Any(l => (l.PrimaryEmail == model.Email || l.SecondaryEmail == model.Email) & l.LeadId !=model.LeadId))
                {
                    lead = context.Lead.FirstOrDefault(l => l.IsActive & l.LeadId == model.LeadId);
                    address = context.Address.FirstOrDefault(a => a.IsActive & a.AddressId == lead.ServiceAddressId);
                    AssignLeadValues(model, address, lead);
                    context.SaveChanges();
                    AssignAddressValues(model, address);
                    context.SaveChanges();
                }
                else
                    return false;
            }
            if (options == ContractOptions.GenerateContract)
            {
                var user = context.AspNetUsers.FirstOrDefault(u => u.IsActive & u.Id == model.SalesRepId);
                _methods.ContractReturn(lead, address, user, null, true);
                SaveLeadLeadStatusMapping(lead, LeadContractStatus.ContractSent);
                return true;
            }
            else if (options == ContractOptions.SendInfo)
            {
                if (lead != null) _methods.SendInformation(lead.LeadId);
                SaveLeadLeadStatusMapping(lead, LeadContractStatus.CotractInformationSent);
                return true;
            }
            return true;
        }

        private void SaveLeadLeadStatusMapping(SunshareDatabaseD2DLiveCopy.Lead lead, int leadStatus)
        {
            var currentLeadStatus = context.LeadLeadStatusMapping.FirstOrDefault(l => l.LeadId == lead.LeadId & l.IsCurrent);
            if (currentLeadStatus != null)
            {
                currentLeadStatus.IsCurrent = false;
                context.SaveChanges();
            }
            var leadStatusMapping = context.LeadLeadStatusMapping.Add(new SunshareDatabaseD2DLiveCopy.LeadLeadStatusMapping()
            {
                LeadStatusId = leadStatus,
                LeadId = lead.LeadId,
                IsCurrent = true,
                UpdatedDate = DateTime.Now
            });
            context.SaveChanges();
        }

        private void SaveLeadAssigningHistory(int leadId,int userId)
        {
            var leadUserMappingHistory = context.LeadUserMappingHistory.Add(new SunshareDatabaseD2DLiveCopy.LeadUserMappingHistory()
            {
                LeadId = leadId,
                AspNetUserId = userId
            });
            context.SaveChanges();
        }

        private SunshareDatabaseD2DLiveCopy.Lead SaveLead(LeadViewModel model, SunshareDatabaseD2DLiveCopy.Address address)
        {
            var lead = context.Lead.Add(new SunshareDatabaseD2DLiveCopy.Lead());
            AssignLeadValues(model, address, lead);
            context.SaveChanges();
            return lead;
        }

        public void LogCall(string callComment,int leadId,int callStatusId,DateTime? callDate=null)
        {
            context.CallLog.Add(new SunshareDatabaseD2DLiveCopy.CallLog
            {
                CallComment = callComment,
                LeadId = leadId,
                IsDuplicateCall = false,
                CallStatusId = callStatusId
            });
            var callLog = context.SaveChanges();
            if (callStatusId == 2)
            {

                context.CallAppointment.Add(new SunshareDatabaseD2DLiveCopy.CallAppointment
                {
                    CallLogId = callLog,
                    DateToCall = callDate ?? DateTime.Today
                });
                context.SaveChanges();
            }
        }

        private static void AssignLeadValues(LeadViewModel model, SunshareDatabaseD2DLiveCopy.Address address, SunshareDatabaseD2DLiveCopy.Lead lead)
        {
            lead.IsActive = true;
            lead.AspNetUsersId = model.SalesRepId;
            lead.ServiceAddressId = address.AddressId;
            lead.FirstName = model.FirstName;
            lead.LastName = model.LastName;
            lead.DateOfBirth = model.DateOfBirth.HasValue?model.DateOfBirth.Value.ToString(CultureInfo.InvariantCulture):null;
            lead.PrimaryEmail = model.Email;
            lead.CampaignId = model.CampaignId;
            lead.RefferedBy = model.ReferedBy;
            lead.LeadRankingId = model.LeadRankingId;
            lead.HomePhoneNo = model.Phone.ToString();
            lead.UtilityAccountNo = model.UtilityAccountNumber;
            lead.LeadSourceId = model.LeadSourceId;
            lead.SubscriptionPercentage = Convert.ToInt32(model.Subscription);
            lead.UtilityCompanyName = model.UtilityProvider;
        }

        private void SaveLeadAddressMapping(SunshareDatabaseD2DLiveCopy.Address address, int salesRepId)
        {
            var addressUserMapping = context.AddressUserMapping.Add(new SunshareDatabaseD2DLiveCopy.AddressUserMapping
            {
                IsActive = true,
                UserId = salesRepId,
                AddressId = address.AddressId,
                VisitStatusId = LeadVisitStatus.Others
            }
                );
            context.SaveChanges();
        }

        private SunshareDatabaseD2DLiveCopy.Address SaveLeadAddress(LeadViewModel model)
        {
            var address = context.Address.Add(new SunshareDatabaseD2DLiveCopy.Address() );
            AssignAddressValues(model, address);
            context.SaveChanges();
            return address;
        }

        private void AssignAddressValues(LeadViewModel model, SunshareDatabaseD2DLiveCopy.Address address)
        {
            address.IsActive = true;
            address.AppartmentNo = model.ServiceApartmentNo;
            address.AddressInformation = model.ServiceAddress;
            address.AddressSourceId = LeadAddressSources.TeleSales; //TODO: has to be in contrller get method
            address.County = context.County.FirstOrDefault(m => m.CountyId == model.SerCountyId)?.CountyName;
            address.City = model.ServiceCity;
            address.State = context.State.FirstOrDefault(s => s.StateId == model.SerStateId)?.StateName;
            address.Zipcode = Convert.ToString(model.SerZipCode);
        }

        public int GetLeadContractStatus(int leadId)
        {
            var method = new Methods();
            return method.UpdateLeadDocuSignStatus(leadId);
        }
    }
}
