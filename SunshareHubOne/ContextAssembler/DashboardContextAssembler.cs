﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SunshareHubOne.Models;
using SunshareDatabase;

namespace SunshareHubOne.ContextAssembler
{
    public interface IDashboardContextAssembler
    {
        void AssignLeadToUser(RepInformation model);
    }
    public class DashboardContextAssembler : IDashboardContextAssembler
    {
        private SunshareDatabaseContext context;

        public DashboardContextAssembler()
        {
            context = new SunshareDatabaseContext();
        }
        public void AssignLeadToUser(RepInformation modal)
        {
            var lead= context.Leads.FirstOrDefault(l => l.LeadId == modal.LeadId);
            lead.AspNetUsersId = modal.SelectedRepId;
            context.SaveChanges();
            var leadUserMappingHistory = context.AssignmentHistory.Add(new LeadUserMappingHistory()
            {
                LeadId = modal.LeadId,
                AspNetUserId = modal.SalesRepId
            });
            context.SaveChanges();
        }

        public void AssignLeadToUser(int leadId,int salesRepId)
        {
            var lead = context.Leads.FirstOrDefault(l => l.LeadId == leadId);
            lead.AspNetUsersId = salesRepId;
            context.SaveChanges();
            var leadUserMappingHistory = context.AssignmentHistory.Add(new LeadUserMappingHistory()
            {
                LeadId = leadId,
                AspNetUserId = salesRepId
            });
            context.SaveChanges();
        }
    }
}
