﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace SunshareHubOne.Controls
{

    public static class ToastsHelperExtension
    {
        public static HtmlString Toasts(this HtmlHelper helper)
        {
            return helper.Toasts("toaster");
        }

        
        public static HtmlString Toasts(this HtmlHelper helper, string id)
        {
            var messages = GetToasts();
            return BuildToastContainer(id, messages);
        }

        private static IEnumerable<ToastMessage> GetToasts()
        {
            var toasts = new ToastMessages();
            //UnityConfig.RegisterTypes(toasts);
            var messages = toasts.Messages;
            

            return messages;
        }

        private static HtmlString BuildToastContainer(string id, IEnumerable<ToastMessage> messages)
        {
            // Get the main DIV
            var toastHost = new TagBuilder("div");
            toastHost.AddCssClass("toast-host");

            if (!String.IsNullOrEmpty(id))
                toastHost.Attributes["id"] = id;

            AddToastsTo(toastHost, messages);
            return new HtmlString(toastHost.ToString());
        }

        private static void AddToastsTo(TagBuilder host, IEnumerable<ToastMessage> messages)
        {
            foreach (var toast in messages)
                host.InnerHtml += BuildSingleToast(toast);
        }

        private static string BuildSingleToast(ToastMessage message)
        {
            var toast = new TagBuilder("div");
            toast.AddCssClass("toast");

            if (message.MessageType == ToastMessageType.Message) toast.AddCssClass("message");
            if (message.MessageType == ToastMessageType.Success) toast.AddCssClass("success");
            if (message.MessageType == ToastMessageType.Warning) toast.AddCssClass("warning");
            if (message.MessageType == ToastMessageType.Error) toast.AddCssClass("error");

            if (message.Sticky)
            {
                toast.AddCssClass("sticky");
                toast.InnerHtml = "<div class=\"close\">x</div>";
            }

            toast.InnerHtml += message.Message;
            return toast.ToString();
        }
    }
}
