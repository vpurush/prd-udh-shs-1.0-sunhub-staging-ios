﻿namespace SunshareHubOne.Controls
{
    public enum ToastMessageType
    {
        Message = 0,
        Success = 1,
        Warning = 2,
        Error = 3
    }

    public class ToastMessage
    {
        public ToastMessageType MessageType { get; set; }
        public string Message { get; set; }
        public bool Sticky { get; set; }
    }
}
