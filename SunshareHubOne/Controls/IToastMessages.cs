﻿using System;
using System.Collections.Generic;
using SunshareCommon;

namespace SunshareHubOne.Controls
{

    public interface IToastMessages
    {
        IList<ToastMessage> Messages { get; }
        void Success(string message, params object[] args);
        void Success(bool sticky, string message, params object[] args);
        void Warning(string message, params object[] args);
        void Warning(bool sticky, string message, params object[] args);
        void Error(string message, params object[] args);
        void Error(bool sticky, string message, params object[] args);

        void Message(string message, params object[] args);

       void Message(bool sticky, string message, params object[] args);
    }

    public class ToastMessages : IToastMessages
    {
        public IList<ToastMessage> Messages { get; private set; }

        public ToastMessages()
        {
            Messages = new List<ToastMessage>();
        }

        private void AddToast(ToastMessageType type, bool sticky, string message, params object[] args)
        {
            Messages.Add(new ToastMessage
            {
                MessageType = type,
                Message = String.Format(message, args),
                Sticky = sticky
            });
        }

        public void Success(string message, params object[] args)
        {
            AddToast(ToastMessageType.Success, false, message, args);
        }

        public void Success(bool sticky, string message, params object[] args)
        {
            AddToast(ToastMessageType.Success, sticky, message, args);
        }

        public void Warning(string message, params object[] args)
        {
            AddToast(ToastMessageType.Warning, false, message, args);
        }

        public void Warning(bool sticky, string message, params object[] args)
        {
            AddToast(ToastMessageType.Warning, sticky, message, args);
        }

        public void Error(string message, params object[] args)
        {
            AddToast(ToastMessageType.Error, false, message, args);
        }

        public void Error(bool sticky, string message, params object[] args)
        {
            AddToast(ToastMessageType.Error, sticky, message, args);
        }

        public void Message(string message, params object[] args)
        {
            AddToast(ToastMessageType.Message, false, message, args);
        }

        public void Message(bool sticky, string message, params object[] args)
        {
            AddToast(ToastMessageType.Message, sticky, message, args);
        }
    }
}
