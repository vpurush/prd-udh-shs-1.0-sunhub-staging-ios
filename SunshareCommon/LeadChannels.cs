﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunshareCommon
{
    public static class LeadChannels
    {
        public static int Sunshare = 1;
        public static int Results = 2;
        public static int PCCW = 3;
        public static int WebEnrollment = 4;
        public static int DoorKnocking = 5;
        public static int ResultsCallCenter = 6;
        public static int PCCWCallCenter = 7;
        public static int WebEnrollmentAuto = 8;
        public static int WebLanding = 9;
        public static int D2D = 10;
    }
}
