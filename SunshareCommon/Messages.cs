﻿namespace SunshareCommon
{
    public static class Messages
    {
        public static string ValidEmailId = "Please enter valid EmailId.";
        public static string ValidPassword = "Please enter valid Password.";
        public static string ValidPartnerId = "Please enter valid PartnerId.";
        public static string ValidEmailandPassword = "Please enter valid EmailId & Password.";

        public static string ValidationSuccess = "User validated successfully.";

        public static string EmailSubjectPasswordReset = "Your password has been reset";

        public static string EmailSendInformation = "Solar Services Information";

        public static string EmailAppointmentSubject = "Appointment Confirmation : Sunshare Solar Community System";

        public static string EmailSendAgreement = "Solar Services Agreement";

        public static string PasswordResetSuccess = "Reset Password has been sent to your regstered Email";

        public static string EmailBodyPasswordReset =
            "Upon validating your request, your password has been changed to {0}";

        public static string PasswordChangedSuccess = "Password has been changed successfully.";
        public static string MailSendingError = "Mail sending error";
        public static string ErrorProvideRequiredValues = "Provide required values";
        public static string ErrorTemplateMapping = "Template mapping failed";
        public static string Success="Success";
        public static string RecordNotFound = "Record not found";
        public static string ErrorDeletingRecord ="Error deleting record";

        public static string LeadCreatedSuccessfully ="Lead created successfully.";
        public static string InfoSentSuccessfully = "Info sent successfully.";
        public static string ContractGeneratedSuccessfully = "Contract sent successfully.";
        public static string LeadWithEmailExisits = "Lead with email already exisit.";

        public static string SuccessTempdata = "UserMessage";
        public static string ErrorTempData = "ErrorMessage";

        public const string SubmitTypeSave = "Save";
        public const string SubmitTypeInfo = "Info";
        public const string SubmitTypeContract = "Contract";
        public const string SubmitTypeClear = "Clear";
    }
}

