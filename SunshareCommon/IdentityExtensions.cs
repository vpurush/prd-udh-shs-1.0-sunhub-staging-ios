﻿using System;
using System.Linq;

namespace SunshareCommon
{
    public static class IdentityExtensions
    {

        public static Efficiency GetEfficientPercentage(this System.Security.Principal.IIdentity identity, string UserID)
        {
            //return "1";
            //var claim = ((System.Security.Claims.ClaimsIdentity)identity).FindFirst("EfficientPercentage");
            using (SunshareDatabase.SunshareDatabaseContext context = new SunshareDatabase.SunshareDatabaseContext())
            {
                context.Database.Connection.Open();
                var createdLead = context.Leads.Count(c => c.AspNetUsersId.ToString() == UserID);

                var convertedLead = context.Leads.Count(c => c.AspNetUsersId.ToString() == UserID & context.LeadLeadStatusMappings.FirstOrDefault(l => l.LeadId == c.LeadId & l.IsCurrent).LeadStatusId == LeadContractStatus.ContractSigned);

                // Test for null to avoid issues during local testing


                return new Efficiency() { CreatedLead = createdLead, ConvertedLead = convertedLead };
            }

        }
        public static string GetDisplayName(this System.Security.Principal.IIdentity identity, string UserID)
        {
            using (SunshareDatabase.SunshareDatabaseContext context = new SunshareDatabase.SunshareDatabaseContext())
            {
                var User = context.Users.FirstOrDefault(u => u.Id.ToString() == UserID);
                return string.Format("{0} {1}", User.FirstName,User.LastName);
            }
        }
    }
    public class Efficiency
    {
        public int CreatedLead { get; set; }
        public int ConvertedLead { get; set; }
        public string EfficiencyPercentage
        {
            get
            {
                if (CreatedLead == 0 || ConvertedLead == 0)
                {
                    return "N/A";
                }
                else
                {
                    return ((decimal)(ConvertedLead / CreatedLead) * 100).ToString();
                }
            }
        }
    }

}
