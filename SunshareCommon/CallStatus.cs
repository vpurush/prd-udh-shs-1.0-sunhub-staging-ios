﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunshareCommon
{
    public static class CallStatuses
    {
        public const int DontCall = 1;
       public const int CallBack = 2;
        public const int ClosedWon = 3;
        public const int ClosedLost = 4;
        public const int Incoming = 5;
    }
}
