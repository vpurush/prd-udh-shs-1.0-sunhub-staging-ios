﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunshareCommon
{
    public static class LeadVisitStatus
    {
        public static int NotatHome = 1;
        public static int NotInterested = 2;
        public static int GoBack = 3;
        public static int Appointment = 4;
        public static int CloseSale = 5;
        public static int Others = 6;
        public static int CustomerInterested = 7;
    }
}
