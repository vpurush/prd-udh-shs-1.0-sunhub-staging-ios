﻿using DocuSign.eSign.Api;
using DocuSign.eSign.Client;
using DocuSign.eSign.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Hosting;

namespace SunshareCommon
{
    public class CoreRecipes
    {
        // Integrator Key (aka API key) is needed to authenticate your API calls.  This is an application-wide key
        private static string IntegratorKey = ConfigurationManager.AppSettings["docuSignIntegratorKey"];// "DOCU-4dafecf7-404f-4de9-a1d0-fdab3d74f00a"; //"THRE-3aed1fd7-868c-4c36-b227-6c9a588288b2";//"WHYN-215daa24-54a7-4c9f-ba3c-8aa5f2dc37cf";//"TSED-7cdadf48-8a8b-4d02-a4ad-73b6ec983041";// "TEST-587ec96d-ee37-4e32-abb6-fd53ea10df50";// "NETS-00f65c38-3457-4db8-a093-78b5a134c0f6";// "DOCU-4dafecf7-404f-4de9-a1d0-fdab3d74f00a";//"[INTEGRATOR_KEY]";//DOCU-4dafecf7-404f-4de9-a1d0-fdab3d74f00a
        private readonly string _accountId;
        public CoreRecipes(string userName, string password)
        {
            ConfigureApiClient("https://demo.docusign.net/restapi");
            _accountId = LoginApi(userName, password);
        }

        public LoginAccount GetAccountId(string userName, string password)
        {
            return LoginApiUserId(userName, password);
        }
        public EnvelopeSummary RequestSignatureOnDocument(string cusEmail,
            string cusName, IEnumerable<string> filePaths, string repEmail, IEnumerable<string> fileNames, string utilityAccountNo = null, bool isCustomer = false)
        {
            var envDef = new EnvelopeDefinition
            {
                EmailSubject = Messages.EmailSendAgreement,
                Documents = new List<Document>()
            };
            int[] documentId = { 1 };
            foreach (var doc in filePaths.Select(File.ReadAllBytes).Select(fileBytes => new Document
            {
                DocumentId = Convert.ToString(documentId[0]),
                DocumentBase64 = Convert.ToBase64String(fileBytes),
                Name = $"{fileNames.ToList()[documentId[0] - 1]}.{"pdf"}",
            }))
            {
                documentId[0] = documentId[0] + 1;
                envDef.Documents.Add(doc);
            }
            Text accountNoOne = null;
            Text accountNoTwo = null;
            if (string.IsNullOrEmpty(utilityAccountNo))
            {
                accountNoOne = new Text()
                {
                    DocumentId = "1",
                    PageNumber = "2",
                    XPosition = "170",
                    YPosition = "315",
                    Required = "true",
                    Locked = "false",
                    ValidationPattern = "[0-9]+",
                    Font = "Arial",
                    FontSize = "5",
                    DisableAutoSize = "true",
                    Width = 152,
                    ValidationMessage = "Please enter valid account Number"
                };
                accountNoTwo = new Text()
                {
                    DocumentId = "2",
                    PageNumber = "2",
                    XPosition = "345",
                    YPosition = "645",
                    Required = "true",
                    Locked = "false",
                    ValidationPattern = "[0-9]+",
                    Font = "Arial",
                    FontSize = "5",
                    DisableAutoSize = "true",
                    Width = 152,
                    ValidationMessage = "Please enter valid account Number"
                };
            }

            var signHereOne = new SignHere
            {
                DocumentId = "1",
                PageNumber = "9",
                RecipientId = "2",
                XPosition = "350",
                YPosition = "420"
            };
            var signHereTwo = new SignHere()
            {
                DocumentId = "2",
                PageNumber = "2",
                RecipientId = "2",
                XPosition = "240",
                YPosition = "667"
            };
            var signHereThree = new SignHere()
            {
                DocumentId = "3",
                PageNumber = "3",
                RecipientId = "2",
                XPosition = "170",
                YPosition = "620"
            };

            if (isCustomer)
            {
                var rep = new CarbonCopy()
                {
                    Email = repEmail,
                    Name = "Sunshare User",
                    RecipientId = "2"
                };
                var cus = new Signer()
                {
                    Email = cusEmail,
                    UserId = "1",
                    RecipientId = "1",
                    Name = cusName,
                    Tabs = new Tabs { SignHereTabs = new List<SignHere>(), TextTabs = new List<Text>() }
                };
                if (accountNoOne != null & accountNoTwo != null)
                {
                    cus.Tabs.TextTabs.Add(accountNoOne);
                    cus.Tabs.TextTabs.Add(accountNoTwo);
                }

                cus.Tabs.SignHereTabs.Add(signHereOne);
                cus.Tabs.SignHereTabs.Add(signHereTwo);
                cus.Tabs.SignHereTabs.Add(signHereThree);

                envDef.Recipients = new Recipients() { CarbonCopies = new List<CarbonCopy>() { rep }, Signers = new List<Signer>() { cus } };
            }
            else
            {
                var inPerson = new InPersonSigner()
                {
                    HostEmail = repEmail,
                    UserId = "1",
                    RecipientId = "2",
                    HostName = "Andrew Subash",
                    SignerEmail = cusEmail,
                    SignerName = cusName,
                    RoutingOrder = "1",
                    Tabs = new Tabs { SignHereTabs = new List<SignHere>(), TextTabs = new List<Text>() }
                };
                if (accountNoOne != null & accountNoTwo != null)
                {
                    inPerson.Tabs.TextTabs.Add(accountNoOne);
                    inPerson.Tabs.TextTabs.Add(accountNoTwo);
                }
                inPerson.Tabs.SignHereTabs.Add(signHereOne);
                inPerson.Tabs.SignHereTabs.Add(signHereTwo);
                inPerson.Tabs.SignHereTabs.Add(signHereThree);

                envDef.Recipients = new Recipients { InPersonSigners = new List<InPersonSigner>() { inPerson } };
            }
            //if (subscription == 0)
            //{
            //    var field = host.Tabs.SignHereTabs.FirstOrDefault(r => r.DocumentId == "1" & r.PageNumber == "9");
            //    if (field != null)
            //    {
            //        field. = "true";
            //        field.Locked = "false";
            //        field.ValidationPattern = "[0-9]+";
            //        field.ValidationMessage = "Please enter valid account number";
            //    }
            //}

            var emailFilePath = HostingEnvironment.MapPath("~/FileServer/Upload/ContractEmailTemplate.html");
            if (emailFilePath != null)
            {
                var mailTemplate = File.ReadAllText(emailFilePath).Replace("{0}", cusName);
                envDef.EmailBlurb = mailTemplate;
            }
            envDef.Status = "sent";
            var envelopesApi = new EnvelopesApi();
            var envelopeSummary = envelopesApi.CreateEnvelope(_accountId, envDef);
            return envelopeSummary;
        }

        public Envelope GetEnvelopeInformation(string envelopeId)
        {
            var envelopesApi = new EnvelopesApi();
            var envInfo = envelopesApi.GetEnvelope(_accountId, envelopeId);
            return envInfo;
        }

        //public EnvelopeSummary RequestSignatureFromTemplate(string recipientEmail,
        //    string recipientName, string filePath, string templateId, string templateRoleName)
        //{
        //    var envDef = new EnvelopeDefinition
        //    {
        //        EmailSubject = "[DocuSign C# SDK] - Please sign this doc"
        //    };
        //    var tRole = new TemplateRole
        //    {
        //        Email = recipientEmail,
        //        Name = recipientName,
        //        RoleName = templateRoleName
        //    };

        //    var rolesList = new List<TemplateRole>() {tRole};
        //    envDef.TemplateRoles = rolesList;
        //    envDef.TemplateId = templateId;
        //    envDef.Status = "sent";
        //    var envelopesApi = new EnvelopesApi();
        //    var envelopeSummary = envelopesApi.CreateEnvelope(accountId, envDef);
        //    return envelopeSummary;
        //} 
        //public Recipients ListRecipients(string userName, string password, string envelopeId)
        //{
        //    var envelopesApi = new EnvelopesApi();
        //    var recips = envelopesApi.ListRecipients(_accountId, envelopeId);
        //    return recips;

        //} 
        //public EnvelopesInformation ListEnvelopes(string userName, string password)
        //{
        //    ConfigureApiClient("https://demo.docusign.net/restapi");
        //    var accountId = LoginApi(userName, password);
        //    var fromDate = DateTime.UtcNow;
        //    fromDate = fromDate.AddDays(-30);
        //    var fromDateStr = fromDate.ToString("o");
        //    var options = new EnvelopesApi.ListStatusChangesOptions()
        //    {
        //        count = "10",
        //        fromDate = fromDateStr
        //    };
        //    var envelopesApi = new EnvelopesApi();
        //    var envelopes = envelopesApi.ListStatusChanges(accountId, options);
        //    return envelopes;

        //} 
        //public void ListDocumentsAndDownload(string userName, string password, string envelopeId)
        //{
        //    ConfigureApiClient("https://demo.docusign.net/restapi");
        //    var accountId = LoginApi(userName, password);
        //    var envelopesApi = new EnvelopesApi();
        //    var docsList = envelopesApi.ListDocuments(accountId, envelopeId);
        //    var docCount = docsList.EnvelopeDocuments.Count;
        //    for (var i = 0; i < docCount; i++)
        //    {
        //        var docStream =
        //            (MemoryStream)
        //                envelopesApi.GetDocument(accountId, envelopeId, docsList.EnvelopeDocuments[i].DocumentId);
        //        var filePath = Path.GetTempPath() + Path.GetRandomFileName() + ".pdf";
        //        var fs = new FileStream(filePath, FileMode.Create);
        //        docStream.Seek(0, SeekOrigin.Begin);
        //        docStream.CopyTo(fs);
        //        fs.Close();
        //    }

        //} 
        //public ViewUrl CreateEmbeddedSendingView(string userName,string password,string recipientName,string recipientEmail,string documentPath)
        //{
        //    ConfigureApiClient("https://demo.docusign.net/restapi");
        //    var accountId = LoginApi(userName, password);
        //    var fileBytes = File.ReadAllBytes(documentPath);
        //    var envDef = new EnvelopeDefinition {EmailSubject = "[DocuSign C# SDK] - Please sign this doc"};
        //    var doc = new Document
        //    {
        //        DocumentBase64 = Convert.ToBase64String(fileBytes),
        //        Name = "TestFile.pdf",
        //        DocumentId = "1"
        //    };
        //    envDef.Documents = new List<Document> {doc};
        //    var signer = new Signer
        //    {
        //        Email = recipientEmail,
        //        Name = recipientName,
        //        RecipientId = "1",
        //        Tabs = new Tabs {SignHereTabs = new List<SignHere>()}
        //    };
        //    var signHere = new SignHere
        //    {
        //        DocumentId = "1",
        //        PageNumber = "1",
        //        RecipientId = "1",
        //        XPosition = "100",
        //        YPosition = "100"
        //    };
        //    signer.Tabs.SignHereTabs.Add(signHere);
        //    envDef.Recipients = new Recipients {Signers = new List<Signer> {signer}};
        //    envDef.Status = "created";
        //    var envelopesApi = new EnvelopesApi();
        //    var envelopeSummary = envelopesApi.CreateEnvelope(accountId, envDef);
        //    var options = new ReturnUrlRequest {ReturnUrl = "https://www.docusign.com/devcenter"};
        //    var senderView = envelopesApi.CreateSenderView(accountId, envelopeSummary.EnvelopeId, options);
        //    System.Diagnostics.Process.Start(senderView.Url);
        //    return senderView;
        //} 
        //public ViewUrl CreateEmbeddedSigningView(string userName,string password,string receipientName,string recipientEmail,string documentPath)
        //{
        //    ConfigureApiClient("https://demo.docusign.net/restapi");
        //    var accountId = LoginApi(userName, password);
        //    var fileBytes = File.ReadAllBytes(documentPath);
        //    var envDef = new EnvelopeDefinition
        //    {
        //        EmailSubject = "[DocuSign C# SDK] - Please sign this doc"
        //    };
        //    var doc = new Document
        //    {
        //        DocumentBase64 = Convert.ToBase64String(fileBytes),
        //        Name = "TestFile.pdf",
        //        DocumentId = "1"
        //    };
        //    envDef.Documents = new List<Document> {doc};
        //    var signer = new Signer
        //    {
        //        Email = recipientEmail,
        //        Name = recipientEmail,
        //        RecipientId = "1",
        //        ClientUserId = "1234",
        //        Tabs = new Tabs {SignHereTabs = new List<SignHere>()}
        //    };
        //    var signHere = new SignHere
        //    {
        //        DocumentId = "1",
        //        PageNumber = "1",
        //        RecipientId = "1",
        //        XPosition = "100",
        //        YPosition = "100"
        //    };
        //    signer.Tabs.SignHereTabs.Add(signHere);
        //    envDef.Recipients = new Recipients {Signers = new List<Signer> {signer}};
        //    envDef.Status = "sent";
        //    var envelopesApi = new EnvelopesApi();
        //    var envelopeSummary = envelopesApi.CreateEnvelope(accountId, envDef);
        //    var viewOptions = new RecipientViewRequest()
        //    {
        //        ReturnUrl = "https://www.docusign.com/devcenter",
        //        ClientUserId = "1234", 
        //        AuthenticationMethod = "email",
        //        UserName = envDef.Recipients.Signers[0].Name,
        //        Email = envDef.Recipients.Signers[0].Email
        //    };
        //    var recipientView = envelopesApi.CreateRecipientView(accountId, envelopeSummary.EnvelopeId, viewOptions);
        //    System.Diagnostics.Process.Start(recipientView.Url);
        //    return recipientView;
        //} 
        //public ViewUrl CreateEmbeddedConsoleView(string userName,string password)
        //{
        //    ConfigureApiClient("https://demo.docusign.net/restapi");
        //    var accountId = LoginApi(userName, password);
        //    var envelopesApi = new EnvelopesApi();
        //    var viewUrl = envelopesApi.CreateConsoleView(accountId, null);
        //    System.Diagnostics.Process.Start(viewUrl.Url);
        //    return viewUrl;
        //}

        private static void ConfigureApiClient(string basePath)
        {
            var apiClient = new ApiClient(basePath);
            DocuSign.eSign.Client.Configuration.Default.ApiClient = apiClient;
        }
        private static string LoginApi(string userName, string password)
        {
            var authHeader = "{\"Username\":\"" + userName + "\", \"Password\":\"" + password +
                             "\", \"IntegratorKey\":\"" + IntegratorKey + "\"}";
            DocuSign.eSign.Client.Configuration.Default.DefaultHeader.Remove("X-DocuSign-Authentication");
            DocuSign.eSign.Client.Configuration.Default.AddDefaultHeader("X-DocuSign-Authentication", authHeader);
            var authApi = new AuthenticationApi();
            var loginInfo = authApi.Login();

            var accountId =
                (from loginAcct in loginInfo.LoginAccounts
                 where loginAcct.IsDefault == "true"
                 select loginAcct.AccountId).FirstOrDefault() ??
                loginInfo.LoginAccounts[0].AccountId;
            return accountId;
        }
        private static LoginAccount LoginApiUserId(string userName, string password)
        {
            var authHeader = "{\"Username\":\"" + userName + "\", \"Password\":\"" + password +
                             "\", \"IntegratorKey\":\"" + IntegratorKey + "\"}";
            DocuSign.eSign.Client.Configuration.Default.DefaultHeader.Remove("X-DocuSign-Authentication");
            DocuSign.eSign.Client.Configuration.Default.AddDefaultHeader("X-DocuSign-Authentication", authHeader);
            var authApi = new AuthenticationApi();
            var loginInfo = authApi.Login();

            return loginInfo.LoginAccounts.FirstOrDefault(l => l.IsDefault == "true");
            //return accountId;
        }
    }
}




