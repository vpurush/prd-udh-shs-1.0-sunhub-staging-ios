﻿using iTextSharp.text.pdf;
using SunshareDatabaseD2DLiveCopy;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web.Hosting;

namespace SunshareCommon
{
    public class Methods
    {
        //readonly SunshareDatabaseContext _context = new SunshareDatabaseContext();
        readonly SunshareContext _context = new SunshareContext();
        private string _docuSignUserName = ConfigurationManager.AppSettings["docuSignUserName"];// "telesalessupport@mysunshare.com";
        private string _docuSignPassword = ConfigurationManager.AppSettings["docuSignPassword"];// "Mobility#2016";
        readonly CoreRecipes _docuSign;

        public Methods()
        {
            _docuSign = new CoreRecipes(_docuSignUserName, _docuSignPassword);
        }
        public ContractReturn ContractReturn(SunshareDatabaseD2DLiveCopy.Lead userLead, SunshareDatabaseD2DLiveCopy.Address userAddress, SunshareDatabaseD2DLiveCopy.AspNetUsers rep,
            SunshareDatabaseD2DLiveCopy.AddressUserMapping mapping = null, bool isCustomer = false)
        {
            var documentName =
                $"pdf_SunShare_TEXT_{userLead.LeadId}_{userLead.FirstName}_{userLead.LastName}_{userAddress?.Zipcode}_{$"{DateTime.Now.Month}{DateTime.Now.Day}{DateTime.Now.Year}"}_{$"{DateTime.Now.Hour}{DateTime.Now.Minute}{DateTime.Now.Second}"}";
            var documentPath = GenerateDocument(documentName, userLead, userAddress);
            if (rep == null) return new ContractReturn { ReturnMsg = "Something went wrong", ReturnCode = 401 };
            var envelopesummary = _docuSign.RequestSignatureOnDocument(userLead.PrimaryEmail,
                $"{userLead.FirstName} {userLead.LastName}", documentPath.DocumentPaths, rep.Email, documentPath.DocumentNames,
                userLead.UtilityAccountNo, isCustomer);
            // var envelopesummary = _docuSign.RequestSignatureOnDocument(userLead.PrimaryEmail, "ILS TEST", documentPath.documentPaths, rep.Email, documentPath.documentNames, userLead.UtilityAccountNo);
            //TODO: hardcodes needs tobe moved to config.

            var lead = _context.Lead.FirstOrDefault(l => l.IsActive & l.LeadId == userLead.LeadId);
            if (lead != null) lead.EnvelopeId = envelopesummary.EnvelopeId;
            var currentLeadLeadStatusMapping =
                _context.LeadLeadStatusMapping.FirstOrDefault(l => l.IsCurrent & l.LeadId == userLead.LeadId);
            if (currentLeadLeadStatusMapping != null) currentLeadLeadStatusMapping.IsCurrent = false;
            _context.LeadLeadStatusMapping.Add(new SunshareDatabaseD2DLiveCopy.LeadLeadStatusMapping()
            {
                IsCurrent = true,
                LeadId = userLead.LeadId,
                LeadStatusId = LeadContractStatus.ContractSent,
                UpdatedDate = DateTime.Today
            });

            //userLead.LeadStatusId = ;
            _context.SaveChanges();

            return new ContractReturn
            {
                ReturnMsg = Messages.Success,
                ReturnCode = 200,
                EnvelopeId = envelopesummary.EnvelopeId,
                LogId = Convert.ToString(mapping?.AddressUserMappingId ?? 0)
            };
        }

        public LogReturn SendInformation(SunshareDatabaseD2DLiveCopy.Lead userLead)
        {
            var filePath = HostingEnvironment.MapPath("~/FileServer/Upload/SendInformationTemplate.html");
            if (filePath == null) return new LogReturn { ReturnMsg = Messages.ErrorTemplateMapping, ReturnCode = 401 };

            var mailTemplate = File.ReadAllText(filePath).Replace("{0}", userLead.FirstName).Replace("{1}", userLead.LastName);
            SendEmail(userLead.PrimaryEmail, Convert.ToString(mailTemplate), Messages.EmailSendInformation, true);
            return new LogReturn { ReturnMsg = Messages.Success, ReturnCode = 200 };
        }
        public ContractReturn SendInformationC(SunshareDatabaseD2DLiveCopy.Lead userLead)
        {
            var filePath = HostingEnvironment.MapPath("~/FileServer/Upload/SendInformationTemplate.html");
            if (filePath == null) return new ContractReturn { ReturnMsg = Messages.ErrorTemplateMapping, ReturnCode = 401 };

            var mailTemplate = File.ReadAllText(filePath).Replace("{0}", userLead.FirstName).Replace("{1}", userLead.LastName);
            SendEmail(userLead.PrimaryEmail, Convert.ToString(mailTemplate), Messages.EmailSendInformation, true);
            return new ContractReturn { ReturnMsg = Messages.Success, ReturnCode = 200 };
        }

        public static bool SendEmail(string toEmail, string message, string subject, bool isHtml = false)
        {
            try
            {
                var mail = new MailMessage();
                var smtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccountName"]);
                mail.To.Add(toEmail);
                mail.Subject = subject;
                mail.Body = message;
                mail.IsBodyHtml = isHtml;
                smtpServer.Port = 587;
                //smtpServer.Port = 465;
                smtpServer.Timeout = 10000;
                //smtpServer.UseDefaultCredentials = false;
                //smtpServer.Credentials = new System.Net.NetworkCredential("telesalessupport@sunsharecorp.com","SunShare1234!");//SunShare1!");
                smtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailUserName"], ConfigurationManager.AppSettings["EmailPassword"]);//SunShare1!");
                //smtpServer.Credentials = new System.Net.NetworkCredential("subashandrew@gmail.com", "");
                smtpServer.EnableSsl = true;
                smtpServer.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void LogTimeTaken(string time)
        {
            using (var Lfile = File.AppendText("C:/log_stage.txt"))
            {
                Lfile.WriteLine(time);
            }
        }

        public void UpdateDocuSignStatus(int userId)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            Stopwatch stopwatch1 = Stopwatch.StartNew();
            //var leads = _context.Lead.Where(l => l.IsActive & l.AspNetUsersId == userId & l.EnvelopeId != "");
            var leads = _context.Database.SqlQuery<SunshareDatabaseD2DLiveCopy.Lead>("GetAgentOutStandingContractLeads @salesRepId", new SqlParameter("salesRepId", userId)).ToList(); //count;//userLeads.Count(l => l.GetLeadStatusId == LeadContractStatus.ContractSent);
            stopwatch1.Stop();
            LogTimeTaken(DateTime.Now.ToString() + "Time Taken to retirve all lead with outstanding contract status " + leads.Count() + " " + stopwatch1.ElapsedMilliseconds.ToString());
            stopwatch1.Restart();
            foreach (var lead in leads)
            {
                Stopwatch TimerForOneLeadUpdate = Stopwatch.StartNew();
                UpdateLeadStatus(lead);
                TimerForOneLeadUpdate.Stop();
                LogTimeTaken(DateTime.Now.ToString() + " Time Taken to retirve and update for single lead " + TimerForOneLeadUpdate.ElapsedMilliseconds.ToString());

                //stopwatch.Stop();
                //LogTimeTaken(DateTime.Now.ToString() + "Time Taken to Complete process for one envelope " + stopwatch.ElapsedMilliseconds.ToString());
                //stopwatch.Restart();
            }
            LogTimeTaken(DateTime.Now.ToString() + " Time Taken to retirve update all envelope status " + stopwatch1.ElapsedMilliseconds.ToString());
            _context.SaveChanges();
        }

        public int UpdateLeadDocuSignStatus(int leadId)
        {
            var lead = _context.Lead.FirstOrDefault(l => l.IsActive & l.LeadId == leadId);
            var status = UpdateLeadStatus(lead);
            _context.SaveChanges();
            return status;
        }

        public int UpdateLeadStatus(SunshareDatabaseD2DLiveCopy.Lead lead)
        {
            var leadLeadStatusMapping = _context.LeadLeadStatusMapping.FirstOrDefault(l => l.IsCurrent & l.LeadId == lead.LeadId);
            if (leadLeadStatusMapping == null ||
                leadLeadStatusMapping.LeadStatusId != LeadContractStatus.ContractSent) return leadLeadStatusMapping?.LeadStatusId ?? 0;
            {
                //if (string.IsNullOrEmpty(lead.EnvelopeId)) return leadLeadStatusMapping.LeadStatusId;
                Stopwatch stopwatch = Stopwatch.StartNew();
                var result = _docuSign.GetEnvelopeInformation(lead.EnvelopeId);
                stopwatch.Stop();
                LogTimeTaken(DateTime.Now.ToString() + "Time Taken to retrive status of Envelope " + lead.EnvelopeId + " " + stopwatch.ElapsedMilliseconds.ToString());
                stopwatch.Restart();
                //var currentLeadLeadStatusMapping =
                //  _context.LeadLeadStatusMapping.FirstOrDefault(l => l.IsCurrent && l.LeadId == lead.LeadId);

                if (result.Status == DocuSignEnvelopeStatus.Completed ||
                    result.Status == DocuSignEnvelopeStatus.Signed)
                {
                    // if (currentLeadLeadStatusMapping.LeadStatusId != LeadContractStatus.ContractSigned)
                    //{
                    _context.LeadLeadStatusMapping.Add(new SunshareDatabaseD2DLiveCopy.LeadLeadStatusMapping()
                    {
                        LeadId = lead.LeadId,
                        LeadStatusId = LeadContractStatus.ContractSigned,
                        IsCurrent = true,
                        UpdatedDate = Convert.ToDateTime(result.CompletedDateTime)
                    });
                    //if (currentLeadLeadStatusMapping != null)
                    //currentLeadLeadStatusMapping.IsCurrent = false;
                    //_context.SaveChanges();
                    leadLeadStatusMapping.IsCurrent = false;
                    //}
                    stopwatch.Stop();
                    LogTimeTaken(DateTime.Now.ToString() + "Time Taken to update DB with status signed " + lead.EnvelopeId + " " + stopwatch.ElapsedMilliseconds.ToString());
                    return LeadContractStatus.ContractSigned;
                }
                else if (result.Status == DocuSignEnvelopeStatus.Declined)
                {
                    _context.LeadLeadStatusMapping.Add(new SunshareDatabaseD2DLiveCopy.LeadLeadStatusMapping()
                    {
                        LeadId = lead.LeadId,
                        LeadStatusId = LeadContractStatus.ContractDeclined,
                        IsCurrent = true,
                        UpdatedDate = Convert.ToDateTime(result.DeclinedDateTime)
                    });
                    //if (currentLeadLeadStatusMapping != null)
                    //currentLeadLeadStatusMapping.IsCurrent = false; //TODO: ugly coding. Duplicate needs to be removed.
                    leadLeadStatusMapping.IsCurrent = false;
                    stopwatch.Stop();
                    LogTimeTaken(DateTime.Now.ToString() + "Time Taken to update DB with status declined " + lead.EnvelopeId + " " + stopwatch.ElapsedMilliseconds.ToString());
                    return LeadContractStatus.ContractDeclined;
                }
                else
                {
                    stopwatch.Stop();
                    LogTimeTaken(DateTime.Now.ToString() + "Time Taken for no change " + lead.EnvelopeId + " " + stopwatch.ElapsedMilliseconds.ToString());
                }
                return 0;
            }
        }

        #region Private Methods
        private static DocumentDetail GenerateDocument(string filename, SunshareDatabaseD2DLiveCopy.Lead customer, SunshareDatabaseD2DLiveCopy.Address customerAddress)
        {
            DocumentDetail resiltDetails = new DocumentDetail();

            var contractReader = new PdfReader(HostingEnvironment.MapPath("~/FileServer/Upload/ContractTemplate.pdf"));
            var contractStamper = new PdfStamper(contractReader, new FileStream(HostingEnvironment.MapPath("~/FileServer/" + filename.Replace("TEXT", "MN_SSA_Form") + ".pdf"), FileMode.Append));
            contractStamper.AcroFields.SetField("Customer Name", $"{customer.FirstName} {customer.LastName}");
            contractStamper.AcroFields.SetField("Generated on", $"{DateTime.Today: MMM d, yyyy}");
            //stamper.AcroFields.SetField("Sunshare", "Sunshare LLC");
            contractStamper.AcroFields.SetField("Mailing Address 1", customerAddress.AddressInformation);
            contractStamper.AcroFields.SetField("Mailing Address 2",
                $"{customerAddress.City}, {customerAddress.State}-{customerAddress.Zipcode}");
            contractStamper.AcroFields.SetField("Email Address", customer.PrimaryEmail);
            contractStamper.AcroFields.SetField("Street", customerAddress.AddressInformation);
            contractStamper.AcroFields.SetField("City", customerAddress.City);
            contractStamper.AcroFields.SetField("County", customerAddress.County);
            contractStamper.AcroFields.SetField("Xcel Account Number", customer.UtilityAccountNo);
            contractStamper.AcroFields.SetField("ssa", customer.SubscriptionPercentage.ToString());
            contractStamper.AcroFields.SetField("Up to", Convert.ToString(customer.SubscriptionPercentage));
            contractStamper.AcroFields.SetField("Date", DateTime.Today.ToString("MM/dd/yyyy"));

            //contractStamper.AcroFields.Fields["Xcel Account Number"].
            contractStamper.Close();
            contractReader.Close();

            resiltDetails.DocumentNames.Add(filename.Replace("TEXT", "MN_Contract_Form"));
            resiltDetails.DocumentPaths.Add(HostingEnvironment.MapPath("~/FileServer/" + filename.Replace("TEXT", "MN_SSA_Form") + ".pdf"));

            var consentReader = new PdfReader(HostingEnvironment.MapPath("~/FileServer/Upload/MN Consent Form.pdf"));
            var consentStamper = new PdfStamper(consentReader, new FileStream(HostingEnvironment.MapPath("~/FileServer/" + filename.Replace("TEXT", "MN_Consent_Form") + ".pdf"), FileMode.Append));
            consentStamper.AcroFields.SetField("FULL SERVICE ADDRESS", $"{customerAddress.AddressInformation}{customerAddress.City}, {customerAddress.State}-{customerAddress.Zipcode}");
            consentStamper.AcroFields.SetField("Customer Name", $"{customer.FirstName} {customer.LastName}");
            consentStamper.AcroFields.SetField("mm", DateTime.Now.Month.ToString());
            consentStamper.AcroFields.SetField("dd", DateTime.Now.Day.ToString());
            consentStamper.AcroFields.SetField("yyyy", DateTime.Now.Year.ToString());
            consentStamper.AcroFields.SetField("Xcel Account Number", customer.UtilityAccountNo);
            consentStamper.Close();
            consentReader.Close();


            resiltDetails.DocumentNames.Add(filename.Replace("TEXT", "MN_Consent_Form"));
            resiltDetails.DocumentPaths.Add(HostingEnvironment.MapPath("~/FileServer/" + filename.Replace("TEXT", "MN_Consent_Form") + ".pdf"));

            var blankReader = new PdfReader(HostingEnvironment.MapPath("~/FileServer/Upload/MN BLANK SAA.pdf"));
            var blankStamper = new PdfStamper(blankReader, new FileStream(HostingEnvironment.MapPath("~/FileServer/" + filename.Replace("TEXT", "MN_Blank_Form") + ".pdf"), FileMode.Append));
            blankStamper.AcroFields.SetField("Customer Name", $"{customer.FirstName} {customer.LastName}");
            blankStamper.AcroFields.SetField("Date", DateTime.Now.ToString("MM/dd/yyyy"));
            blankStamper.Close();
            blankReader.Close();
            resiltDetails.DocumentNames.Add(filename.Replace("TEXT", "MN_Blank_Form"));
            resiltDetails.DocumentPaths.Add(HostingEnvironment.MapPath("~/FileServer/" + filename.Replace("TEXT", "MN_Blank_Form") + ".pdf"));

            return resiltDetails;
            //throw new NotImplementedException();
        }

        internal class DocumentDetail
        {
            public DocumentDetail()
            {
                DocumentPaths = new List<string>();
                DocumentNames = new List<string>();
            }
            public List<string> DocumentPaths { get; private set; }
            public List<string> DocumentNames { get; private set; }
        }
        #endregion

        public LogReturn SendInformation(int leadId)
        {
            return SendInformation(_context.Lead.FirstOrDefault(l => l.IsActive & l.LeadId == leadId));
        }

        public ContractReturn SendInformationC(int leadId)
        {
            return SendInformationC(_context.Lead.FirstOrDefault(l => l.IsActive & l.LeadId == leadId));
        }
    }
}
