﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class VisitStatus : BaseEntity
    {
        public VisitStatus()
        {
        }
        public int VisitStatusId { get; set; }
        public string VisitName { get; set; }
        public bool IsActive { get; set; }

        
    }
}