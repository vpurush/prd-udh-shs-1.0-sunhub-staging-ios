﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class CallLog : BaseEntity
    {
        public CallLog()
        {
        }
        public int CallLogId { get; set; }
        public int LeadId { get; set; }
        public string CallComment { get; set; }
        public string CreatedBy { get; set; }
        public bool? IsDuplicateCall { get; set; }
        public int CallStatusId { get; set; }
        [NotMapped]
        public virtual Lead Lead { get; set; }
        [NotMapped]
        public virtual CallLog Call { get; set; }
        [NotMapped]
        public virtual ICollection<CallAppointment> Appointments { get; set; }
    }
}