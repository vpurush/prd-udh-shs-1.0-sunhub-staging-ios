﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class AddressSource : BaseEntity
    {
        public AddressSource()
        {
        }
        public int AddressSourceId { get; set; }
        public string AddressSourceName { get; set; }
        [NotMapped]
        public virtual ICollection<Address> Address { get; set; }

        
    }
}