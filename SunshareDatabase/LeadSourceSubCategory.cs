﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunshareDatabase
{
    public class LeadSourceSubCategory : BaseEntity
    {
        public LeadSourceSubCategory()
        {
            
        }
        public int LeadSourceSubCategoryId { get; set; }
        public int LeadSourceId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(150)]
        public string LeadSourceSubCategoryName { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("LeadSourceId")]
        public virtual LeadSource LeadSource { get; set; }
    }
}
