﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunshareDatabase
{
    public class CallStatus
    {
             
        public int CallStatusId { get; set; }
        public string CallStatusName { get; set; }
        public bool IsActive { get; set; }
        [NotMapped]
        public ICollection<CallLog> CallLogs { get; set; }
    }
}
