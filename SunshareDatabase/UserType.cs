﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class UserType : BaseEntity
    {
        public UserType()
        {
        }
        public int UserTypeId { get; set; }
        public string UserTypeName { get; set; }

        public virtual ICollection<AspNetUsers> Users { get; set; }

        
    }
}