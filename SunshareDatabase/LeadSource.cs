﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunshareDatabase
{
    public class LeadSource
    {
        public LeadSource()
        {
            
        }

        public int LeadSourceId { get; set; }
        public  string LeadSourceName { get; set; }
        public bool IsActive { get; set; }

        [NotMapped]
        public virtual ICollection<Lead> Leads { get; set; }
    }
}
