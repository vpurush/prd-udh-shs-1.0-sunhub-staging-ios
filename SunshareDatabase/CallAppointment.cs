﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunshareDatabase
{
    public class CallAppointment
    {
        public int CallAppointmentId { get; set; }
        
        public int CallLogId { get; set; }
        public DateTime? DateToCall { get; set; }
        public DateTime CallDate { get; set; }
        
        [ForeignKey("CallLogId")]
        public virtual CallLog CallLog { get; set; }
    }
}
