﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace SunshareDatabase
{
    public class Lead : BaseEntity
    {
        public Lead()
        {
        }
        
        public int LeadId { get; set; }
        //public string LeadName { get; set;} 
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth {get;set;}
        public string UtilityAccountNo { get; set; }
        public string UtilityCompanyName { get; set; }
        public string HomePhoneNo { get; set; }
        public string WorkPhoneNo { get; set; }
        public string MobileNo { get; set; }
        public int ServiceAddressId { get; set; } 
        public int BillingAddressId { get; set; }
        public string RefferedBy { get; set; }
        public int SubscriptionPercentage { get; set; }
        public string SSAType { get; set; }
        public int AspNetUsersId { get; set; }
        public int CampaignId { get; set; }
        public int LeadRankingId { get; set; }

        public int? LeadSourceId { get; set; }
        //[DefaultValue(1)]
        //public int LeadStatusId { get; set; }
        public string EnvelopeId { get; set; }
        public bool IsActive { get; set; }


        [NotMapped]
        public virtual Campaign LeadCampaign { get; set; }
        //public virtual LeadStatus LeadStatus { get; set; }
        [NotMapped]
        public virtual Channel LeadChannel { get; set; }
        [NotMapped]
        public virtual State LeadState { get; set; }
        [NotMapped]
        public virtual County LeadCounty { get; set; }
        [NotMapped]
        public virtual ICollection<CallLog> CallLogs { get; set; }
        [NotMapped]
        public virtual Address LeadServiceAddress { get; set; }
        [NotMapped]
        public virtual Address LeadBillingAddress { get; set; }
        [NotMapped]
        public virtual LeadRanking LeadRanking { get; set; }
        //public virtual AspNetUsers LeadUser { get; set; }
        [NotMapped]
        public virtual ICollection<LeadLeadStatusMapping> LeadLeadStatusMappings { get; set; }
        [NotMapped]
        [ForeignKey("AspNetUsersId")]
        public virtual AspNetUsers LeadRep { get; set; }

        [NotMapped]
        public virtual ICollection<LeadUserMappingHistory> AssignHistory { get; set; }
        //public int GetLeadStatusId
        //{
        //    get
        //    {
        //        return LeadLeadStatusMappings.FirstOrDefault(lm => lm.IsCurrent & lm.LeadId == LeadId)?.LeadStatusId ?? 0;
        //    }
        //}
    }
}