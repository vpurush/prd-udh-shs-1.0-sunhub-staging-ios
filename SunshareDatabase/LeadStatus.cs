﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class LeadStatus : BaseEntity
    {
        public LeadStatus()
        {
        }
        public int LeadStatusId { get; set; }
        public string StatusName { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<Lead> Leads { get; set; }

        
    }
}