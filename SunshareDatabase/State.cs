﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class State : BaseEntity
    {
        public State()
        {
        }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public bool IsActive { get; set; }
        [NotMapped]
        public virtual ICollection<County> Counties { get; set; }
        [NotMapped]
        public virtual ICollection<Lead> Leads { get; set; }        
    }
}