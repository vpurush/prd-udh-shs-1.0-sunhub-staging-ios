﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class Channel : BaseEntity
    {
        public Channel()
        {
        }
        [Key]
        public int ChannelId { get; set; }
        public int? ParentChannelId { get; set; }
        public string ChannelName { get; set; }
        public bool IsActive { get; set; }
        [NotMapped]
        public virtual ICollection<Lead> Leads { get; set; }
        [NotMapped]
        public virtual ICollection<Campaign> Campaigns { get; set; }

        
    }
}