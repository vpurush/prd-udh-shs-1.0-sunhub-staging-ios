﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunshareDatabase
{
    public class AspNetUserLogins : BaseEntity
    {
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        [Key]
        
        public string UserId { get; set; }

        [NotMapped]
        [ForeignKey("UserId")]
        public virtual AspNetUsers User { get; set; }
    }
}
