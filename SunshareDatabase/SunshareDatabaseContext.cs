﻿using Microsoft.AspNet.Identity;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace SunshareDatabase
{
    public class SunshareDatabaseContext : DbContext
    {
        public SunshareDatabaseContext() : base("SunshareDatabase")
        {
        }
        //public DbSet<global::SunshareDatabase.AspNetUsers> Users { get; set; }
        //public DbSet<AspNetRoles> Roles { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<County> Counties { get; set; }
        public DbSet<Lead> Leads { get; set; }
        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<Channel> Channels { get; set; }
        public DbSet<LeadStatus> Status { get; set; }
        public DbSet<CallAppointment> CallAppointment { get; set; }
        public DbSet<CallStatus> CallStatus { get; set; }
        public DbSet<LeadSource> LeadSources { get; set; }
        //public DbSet<Contract> Contracts { get; set; }
        //public DbSet<Contract> Contracts { get; set; }
        public DbSet<Audit> Audits { get; set; }
        public DbSet<CallLog> CallLogs { get; set; }
        public DbSet<Partner> Partners { get; set; }
        //public DbSet<User> OldUsers { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        //public DbSet<UserRole> OldUserRoles { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<AddressSource> AddressSources { get; set; }
        public DbSet<AddressUserMapping> AddressUserMappings { get; set; }
        public DbSet<VisitStatus> VisitStatuses { get; set; }
        public DbSet<LeadRanking> LeadRakings { get; set; }
        public DbSet<LeadLeadStatusMapping> LeadLeadStatusMappings { get; set; }

        public DbSet<AspNetRoles> UserRoles { get; set; }
        public DbSet<AspNetUserRoles> UserInRoles { get; set; }
        public DbSet<AspNetUsers> Users { get; set; }

        public DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }

        public DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }

        public DbSet<LeadUserMappingHistory> AssignmentHistory { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
        public override int SaveChanges()
        {
            try
            {
                var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));
                var currentUsername = HttpContext.Current != null && HttpContext.Current.User != null
                    ? HttpContext.Current.User.Identity.GetUserName()
                    : "Anonymous";
                foreach (var entity in entities)
                {
                    if (entity.State == EntityState.Added)
                    {
                        ((BaseEntity)entity.Entity).DateCreated = DateTime.Now;
                        ((BaseEntity)entity.Entity).UserCreated = currentUsername;
                    }
                    ((BaseEntity)entity.Entity).DateModified = DateTime.Now;
                    ((BaseEntity)entity.Entity).UserModified = currentUsername;
                }
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);
                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);
                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }
    }
}