﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunshareDatabase
{
   public class LeadLeadStatusMapping : BaseEntity
    {
       public LeadLeadStatusMapping()
       {
           
       }
        [NotMapped]
        public virtual Lead Lead { get; set; }
        [NotMapped]
        public virtual LeadStatus LeadStatus {get;set;}

        public int LeadLeadStatusMappingId { get; set; }

        public int LeadId { get; set; }

        public int LeadStatusId { get; set; }

        public DateTime UpdatedDate { get; set; }

        public bool IsCurrent { get; set; }

       
    }
}
