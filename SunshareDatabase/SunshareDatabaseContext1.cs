namespace SunshareDatabase
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SunshareDatabaseContext1 : DbContext
    {
        public SunshareDatabaseContext1()
            : base("name=SunshareDatabaseContext1")
        {
        }

        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<AddressSource> AddressSources { get; set; }
        public virtual DbSet<AddressUserMapping> AddressUserMappings { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRole> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<Audit> Audits { get; set; }
        public virtual DbSet<CallAppointment> CallAppointments { get; set; }
        public virtual DbSet<CallLog> CallLogs { get; set; }
        public virtual DbSet<CallStatu> CallStatus { get; set; }
        public virtual DbSet<Campaign> Campaigns { get; set; }
        public virtual DbSet<Channel> Channels { get; set; }
        public virtual DbSet<County> Counties { get; set; }
        public virtual DbSet<DupliacteContractRequest> DupliacteContractRequests { get; set; }
        public virtual DbSet<Lead> Leads { get; set; }
        public virtual DbSet<LeadLeadStatusMapping> LeadLeadStatusMappings { get; set; }
        public virtual DbSet<LeadRanking> LeadRankings { get; set; }
        public virtual DbSet<LeadSource> LeadSources { get; set; }
        public virtual DbSet<LeadSourceSubCategory> LeadSourceSubCategories { get; set; }
        public virtual DbSet<LeadStatu> LeadStatus { get; set; }
        public virtual DbSet<LeadUserMappingHistory> LeadUserMappingHistories { get; set; }
        public virtual DbSet<Partner> Partners { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<UserType> UserTypes { get; set; }
        public virtual DbSet<VisitStatu> VisitStatus { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>()
                .Property(e => e.AppartmentNo)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.AddressInformation)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.Zipcode)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.County)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.AddressUserMappings)
                .WithRequired(e => e.Address)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.Leads)
                .WithRequired(e => e.Address)
                .HasForeignKey(e => e.BillingAddressId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.Leads1)
                .WithRequired(e => e.Address1)
                .HasForeignKey(e => e.ServiceAddressId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AddressSource>()
                .Property(e => e.AddressSourceName)
                .IsUnicode(false);

            modelBuilder.Entity<AddressSource>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<AddressSource>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<AddressUserMapping>()
                .Property(e => e.Comments)
                .IsUnicode(false);

            modelBuilder.Entity<AddressUserMapping>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<AddressUserMapping>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetRole>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetRole>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetRole>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.AspNetUserRoles)
                .WithRequired(e => e.AspNetRole)
                .HasForeignKey(e => e.RoleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUserClaim>()
                .Property(e => e.ClaimType)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserClaim>()
                .Property(e => e.ClaimValue)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserClaim>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserClaim>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserLogin>()
                .Property(e => e.LoginProvider)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserLogin>()
                .Property(e => e.ProviderKey)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserLogin>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserLogin>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserRole>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUserRole>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUser>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUser>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AddressUserMappings)
                .WithOptional(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUser>()
                .HasOptional(e => e.AspNetUserLogin)
                .WithRequired(e => e.AspNetUser);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserRoles)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.Leads)
                .WithOptional(e => e.AspNetUser)
                .HasForeignKey(e => e.AspNetUsersId);

            modelBuilder.Entity<Audit>()
                .Property(e => e.TableName)
                .IsUnicode(false);

            modelBuilder.Entity<Audit>()
                .Property(e => e.ChangedColumns)
                .IsUnicode(false);

            modelBuilder.Entity<Audit>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<Audit>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<CallAppointment>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<CallAppointment>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<CallLog>()
                .Property(e => e.CallComment)
                .IsUnicode(false);

            modelBuilder.Entity<CallLog>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<CallLog>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<CallLog>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<CallLog>()
                .HasOptional(e => e.CallAppointment)
                .WithRequired(e => e.CallLog);

            modelBuilder.Entity<CallLog>()
                .HasMany(e => e.CallLog1)
                .WithOptional(e => e.CallLog2)
                .HasForeignKey(e => e.Call_CallLogId);

            modelBuilder.Entity<CallStatu>()
                .Property(e => e.CallStatusName)
                .IsUnicode(false);

            modelBuilder.Entity<CallStatu>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<CallStatu>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<CallStatu>()
                .HasMany(e => e.CallLogs)
                .WithRequired(e => e.CallStatu)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Campaign>()
                .Property(e => e.CampaignName)
                .IsUnicode(false);

            modelBuilder.Entity<Campaign>()
                .Property(e => e.AmountSpent)
                .IsUnicode(false);

            modelBuilder.Entity<Campaign>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Campaign>()
                .Property(e => e.TypeOfCampaign)
                .IsUnicode(false);

            modelBuilder.Entity<Campaign>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<Campaign>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<Campaign>()
                .Property(e => e.Incentive)
                .IsUnicode(false);

            modelBuilder.Entity<Campaign>()
                .HasMany(e => e.Leads)
                .WithRequired(e => e.Campaign)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Campaign>()
                .HasMany(e => e.LeadSources)
                .WithMany(e => e.Campaigns)
                .Map(m => m.ToTable("LeadSourceCampaign"));

            modelBuilder.Entity<Channel>()
                .Property(e => e.ChannelName)
                .IsUnicode(false);

            modelBuilder.Entity<Channel>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<Channel>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<County>()
                .Property(e => e.CountyName)
                .IsUnicode(false);

            modelBuilder.Entity<County>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<County>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<DupliacteContractRequest>()
                .Property(e => e.Reason)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.PrimaryEmail)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.SecondaryEmail)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.DateOfBirth)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.UtilityAccountNo)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.UtilityCompanyName)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.HomePhoneNo)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.WorkPhoneNo)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.MobileNo)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.RefferedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.SSAType)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.EnvelopeId)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .Property(e => e.ReasonToClose)
                .IsUnicode(false);

            modelBuilder.Entity<Lead>()
                .HasMany(e => e.CallLogs)
                .WithRequired(e => e.Lead)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lead>()
                .HasMany(e => e.DupliacteContractRequests)
                .WithRequired(e => e.Lead)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lead>()
                .HasMany(e => e.LeadUserMappingHistories)
                .WithRequired(e => e.Lead)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LeadLeadStatusMapping>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<LeadLeadStatusMapping>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<LeadRanking>()
                .Property(e => e.RankingName)
                .IsUnicode(false);

            modelBuilder.Entity<LeadRanking>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<LeadRanking>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<LeadRanking>()
                .HasMany(e => e.Leads)
                .WithRequired(e => e.LeadRanking)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LeadSource>()
                .Property(e => e.LeadSourceName)
                .IsUnicode(false);

            modelBuilder.Entity<LeadSource>()
                .HasMany(e => e.LeadSourceSubCategories)
                .WithRequired(e => e.LeadSource)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LeadSourceSubCategory>()
                .Property(e => e.LeadSourceSubCategoryName)
                .IsUnicode(false);

            modelBuilder.Entity<LeadSourceSubCategory>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<LeadSourceSubCategory>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<LeadStatu>()
                .Property(e => e.StatusName)
                .IsUnicode(false);

            modelBuilder.Entity<LeadStatu>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<LeadStatu>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<LeadStatu>()
                .HasMany(e => e.LeadLeadStatusMappings)
                .WithRequired(e => e.LeadStatu)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LeadUserMappingHistory>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<LeadUserMappingHistory>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<Partner>()
                .Property(e => e.PartnerNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Partner>()
                .Property(e => e.PartnerName)
                .IsUnicode(false);

            modelBuilder.Entity<Partner>()
                .Property(e => e.PartnerLogoUrl)
                .IsUnicode(false);

            modelBuilder.Entity<Partner>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<Partner>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<Partner>()
                .HasMany(e => e.AspNetUsers)
                .WithRequired(e => e.Partner)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<State>()
                .Property(e => e.StateName)
                .IsUnicode(false);

            modelBuilder.Entity<State>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<State>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<UserType>()
                .Property(e => e.UserTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<UserType>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<UserType>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<UserType>()
                .HasMany(e => e.AspNetUsers)
                .WithRequired(e => e.UserType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VisitStatu>()
                .Property(e => e.VisitName)
                .IsUnicode(false);

            modelBuilder.Entity<VisitStatu>()
                .Property(e => e.UserCreated)
                .IsUnicode(false);

            modelBuilder.Entity<VisitStatu>()
                .Property(e => e.UserModified)
                .IsUnicode(false);

            modelBuilder.Entity<VisitStatu>()
                .HasMany(e => e.AddressUserMappings)
                .WithRequired(e => e.VisitStatu)
                .WillCascadeOnDelete(false);
        }
    }
}
