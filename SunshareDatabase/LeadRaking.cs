﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class LeadRanking : BaseEntity
    {
        public LeadRanking()
        {
        }
        [NotMapped]
        public  virtual ICollection<Lead> Leads { get; set; }
        [Key]
        public int LeadRankingId { get; set; }
        public string RankingName { get; set; }

        
    }
}
