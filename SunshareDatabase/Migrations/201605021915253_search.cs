namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class search : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LeadUserMappingHistory",
                c => new
                    {
                        LeadUserMappingHistoryId = c.Int(nullable: false, identity: true),
                        LeadId = c.Int(nullable: false),
                        AspNetUserId = c.Int(nullable: false),
                        DateCreated = c.DateTime(),
                        UserCreated = c.String(),
                        DateModified = c.DateTime(),
                        UserModified = c.String(),
                    })
                .PrimaryKey(t => t.LeadUserMappingHistoryId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LeadUserMappingHistory");
        }
    }
}
