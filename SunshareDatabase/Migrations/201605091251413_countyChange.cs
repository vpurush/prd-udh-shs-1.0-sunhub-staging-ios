namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class countyChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.County", "StateId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.County", "StateId");
        }
    }
}
