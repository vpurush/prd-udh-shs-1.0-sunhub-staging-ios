namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class noloss : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Campaign", "ChannelId", "dbo.Channel");
            DropForeignKey("dbo.Lead", "LeadStatus_LeadStatusId", "dbo.LeadStatus");
            DropForeignKey("dbo.CallAppointment", "CallLogId", "dbo.CallLog");
            DropForeignKey("dbo.Lead", "CampaignId", "dbo.Campaign");
            DropForeignKey("dbo.AspNetUsers", "UserTypeId", "dbo.UserType");
            DropIndex("dbo.CallAppointment", new[] { "CallLogId" });
            DropIndex("dbo.Campaign", new[] { "ChannelId" });
            DropIndex("dbo.Lead", new[] { "LeadStatus_LeadStatusId" });
            //DropColumn("dbo.CallAppointment", "CallAppointmentId");
            //RenameColumn(table: "dbo.CallAppointment", name: "CallLogId", newName: "CallAppointmentId");
            DropPrimaryKey("dbo.AspNetUserLogins");
            DropPrimaryKey("dbo.CallAppointment");
            CreateTable(
                "dbo.DupliacteContractRequest",
                c => new
                {
                    DupliacteContractRequestId = c.Int(nullable: false, identity: true),
                    LeadId = c.Int(nullable: false),
                    Reason = c.String(),
                    IsDecided = c.Boolean(nullable: false),
                    IsApproved = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.DupliacteContractRequestId)
                .ForeignKey("dbo.Lead", t => t.LeadId)
                .Index(t => t.LeadId);

            CreateTable(
                "dbo.LeadSourceSubCategory",
                c => new
                {
                    LeadSourceSubCategoryId = c.Int(nullable: false, identity: true),
                    LeadSourceId = c.Int(nullable: false),
                    LeadSourceSubCategoryName = c.String(),
                    IsActive = c.Boolean(nullable: false),
                    DateCreated = c.DateTime(),
                    UserCreated = c.String(),
                    DateModified = c.DateTime(),
                    UserModified = c.String(),
                })
                .PrimaryKey(t => t.LeadSourceSubCategoryId)
                .ForeignKey("dbo.LeadSource", t => t.LeadSourceId)
                .Index(t => t.LeadSourceId);

            CreateTable(
                "dbo.LeadSourceCampaign",
                c => new
                {
                    LeadSource_LeadSourceId = c.Int(nullable: false),
                    Campaign_CampaignId = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.LeadSource_LeadSourceId, t.Campaign_CampaignId })
                .ForeignKey("dbo.LeadSource", t => t.LeadSource_LeadSourceId)
                .ForeignKey("dbo.Campaign", t => t.Campaign_CampaignId)
                .Index(t => t.LeadSource_LeadSourceId)
                .Index(t => t.Campaign_CampaignId);

            AddColumn("dbo.CallAppointment", "DateCreated", c => c.DateTime());
            AddColumn("dbo.CallAppointment", "UserCreated", c => c.String());
            AddColumn("dbo.CallAppointment", "DateModified", c => c.DateTime());
            AddColumn("dbo.CallAppointment", "UserModified", c => c.String());
            AddColumn("dbo.CallLog", "CallAppointmentId", c => c.Int());
            AddColumn("dbo.CallLog", "Call_CallLogId", c => c.Int());
            AddColumn("dbo.CallStatus", "DateCreated", c => c.DateTime());
            AddColumn("dbo.CallStatus", "UserCreated", c => c.String());
            AddColumn("dbo.CallStatus", "DateModified", c => c.DateTime());
            AddColumn("dbo.CallStatus", "UserModified", c => c.String());
            AddColumn("dbo.Lead", "ChannelId", c => c.Int());
            AddColumn("dbo.Lead", "ReasonToClose", c => c.String());
            AddColumn("dbo.Lead", "IsAssigned", c => c.Boolean(nullable: false));
            AddColumn("dbo.Lead", "AllowDuplicateContract", c => c.Boolean(nullable: false));
            AddColumn("dbo.Lead", "LeadCounty_CountyId", c => c.Int());
            AddColumn("dbo.Lead", "LeadState_StateId", c => c.Int());
            AddColumn("dbo.AspNetUsers", "UserType_UserTypeId", c => c.Int());
            AlterColumn("dbo.AddressUserMapping", "UserId", c => c.Int());
            AlterColumn("dbo.AspNetUserLogins", "UserId", c => c.Int(nullable: false));
            AlterColumn("dbo.LeadUserMappingHistory", "AspNetUserId", c => c.Int());
            AlterColumn("dbo.CallAppointment", "CallAppointmentId", c => c.Int(nullable: false));
            AlterColumn("dbo.CallAppointment", "DateToCall", c => c.DateTime(nullable: false));
            AlterColumn("dbo.CallAppointment", "CallDate", c => c.DateTime());
            AlterColumn("dbo.Lead", "AspNetUsersId", c => c.Int());
            AddPrimaryKey("dbo.AspNetUserLogins", "UserId");
            AddPrimaryKey("dbo.CallAppointment", "CallAppointmentId");
            CreateIndex("dbo.AddressUserMapping", "AddressId");
            CreateIndex("dbo.AddressUserMapping", "UserId");
            CreateIndex("dbo.AddressUserMapping", "VisitStatusId");
            CreateIndex("dbo.AspNetUsers", "UserParentId");
            CreateIndex("dbo.AspNetUsers", "PartnerId");
            CreateIndex("dbo.AspNetUsers", "UserType_UserTypeId");
            CreateIndex("dbo.Lead", "ServiceAddressId");
            CreateIndex("dbo.Lead", "BillingAddressId");
            CreateIndex("dbo.Lead", "AspNetUsersId");
            CreateIndex("dbo.Lead", "ChannelId");
            CreateIndex("dbo.Lead", "LeadRankingId");
            CreateIndex("dbo.Lead", "LeadSourceId");
            CreateIndex("dbo.Lead", "LeadCounty_CountyId");
            CreateIndex("dbo.Lead", "LeadState_StateId");
            CreateIndex("dbo.LeadUserMappingHistory", "LeadId");
            CreateIndex("dbo.LeadUserMappingHistory", "AspNetUserId");
            CreateIndex("dbo.CallLog", "LeadId");
            CreateIndex("dbo.CallLog", "CallStatusId");
            CreateIndex("dbo.CallLog", "Call_CallLogId");
            CreateIndex("dbo.CallAppointment", "CallAppointmentId");
            CreateIndex("dbo.County", "StateId");
            CreateIndex("dbo.LeadLeadStatusMapping", "LeadId");
            CreateIndex("dbo.LeadLeadStatusMapping", "LeadStatusId");
            CreateIndex("dbo.AspNetUserClaims", "UserId");
            CreateIndex("dbo.AspNetUserLogins", "UserId");
            CreateIndex("dbo.AspNetUserRoles", "UserId");
            CreateIndex("dbo.AspNetUserRoles", "RoleId");
            AddForeignKey("dbo.AddressUserMapping", "AddressId", "dbo.Address", "AddressId");
            AddForeignKey("dbo.LeadUserMappingHistory", "LeadId", "dbo.Lead", "LeadId");
            AddForeignKey("dbo.LeadUserMappingHistory", "AspNetUserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.CallLog", "Call_CallLogId", "dbo.CallLog", "CallLogId");
            AddForeignKey("dbo.CallLog", "CallStatusId", "dbo.CallStatus", "CallStatusId");
            AddForeignKey("dbo.CallLog", "LeadId", "dbo.Lead", "LeadId");
            AddForeignKey("dbo.Lead", "BillingAddressId", "dbo.Address", "AddressId");
            AddForeignKey("dbo.Lead", "LeadSourceId", "dbo.LeadSource", "LeadSourceId");
            AddForeignKey("dbo.Lead", "ChannelId", "dbo.Channel", "ChannelId");
            //AddForeignKey("dbo.County", "StateId", "dbo.State", "StateId");
            AddForeignKey("dbo.Lead", "LeadCounty_CountyId", "dbo.County", "CountyId");
            //AddForeignKey("dbo.LeadLeadStatusMapping", "LeadId", "dbo.Lead", "LeadId");
            AddForeignKey("dbo.LeadLeadStatusMapping", "LeadStatusId", "dbo.LeadStatus", "LeadStatusId");
            AddForeignKey("dbo.Lead", "LeadRankingId", "dbo.LeadRanking", "LeadRankingId");
            AddForeignKey("dbo.Lead", "AspNetUsersId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Lead", "ServiceAddressId", "dbo.Address", "AddressId");
            AddForeignKey("dbo.Lead", "LeadState_StateId", "dbo.State", "StateId");
            AddForeignKey("dbo.AspNetUsers", "PartnerId", "dbo.Partner", "PartnerId");
            //AddForeignKey("dbo.AspNetUsers", "UserParentId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUsers", "UserType_UserTypeId", "dbo.UserType", "UserTypeId");
            AddForeignKey("dbo.AddressUserMapping", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AddressUserMapping", "VisitStatusId", "dbo.VisitStatus", "VisitStatusId");
            AddForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles", "Id");
            AddForeignKey("dbo.CallAppointment", "CallAppointmentId", "dbo.CallLog", "CallLogId");
            AddForeignKey("dbo.Lead", "CampaignId", "dbo.Campaign", "CampaignId");
            AddForeignKey("dbo.AspNetUsers", "UserTypeId", "dbo.UserType", "UserTypeId");
            DropColumn("dbo.Campaign", "ChannelId");
            //
        }

        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AddressUserMapping", "VisitStatusId", "dbo.VisitStatus");
            DropForeignKey("dbo.AddressUserMapping", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "UserType_UserTypeId", "dbo.UserType");
            DropForeignKey("dbo.AspNetUsers", "UserParentId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "UserTypeId", "dbo.UserType");
            DropForeignKey("dbo.AspNetUsers", "PartnerId", "dbo.Partner");
            DropForeignKey("dbo.Lead", "LeadState_StateId", "dbo.State");
            DropForeignKey("dbo.Lead", "ServiceAddressId", "dbo.Address");
            DropForeignKey("dbo.Lead", "AspNetUsersId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Lead", "LeadRankingId", "dbo.LeadRanking");
            DropForeignKey("dbo.LeadLeadStatusMapping", "LeadStatusId", "dbo.LeadStatus");
            DropForeignKey("dbo.LeadLeadStatusMapping", "LeadId", "dbo.Lead");
            DropForeignKey("dbo.Lead", "LeadCounty_CountyId", "dbo.County");
            DropForeignKey("dbo.County", "StateId", "dbo.State");
            DropForeignKey("dbo.Lead", "ChannelId", "dbo.Channel");
            DropForeignKey("dbo.LeadSourceSubCategory", "LeadSourceId", "dbo.LeadSource");
            DropForeignKey("dbo.Lead", "LeadSourceId", "dbo.LeadSource");
            DropForeignKey("dbo.LeadSourceCampaign", "Campaign_CampaignId", "dbo.Campaign");
            DropForeignKey("dbo.LeadSourceCampaign", "LeadSource_LeadSourceId", "dbo.LeadSource");
            DropForeignKey("dbo.Lead", "CampaignId", "dbo.Campaign");
            DropForeignKey("dbo.Lead", "BillingAddressId", "dbo.Address");
            DropForeignKey("dbo.DupliacteContractRequest", "LeadId", "dbo.Lead");
            DropForeignKey("dbo.CallLog", "LeadId", "dbo.Lead");
            DropForeignKey("dbo.CallLog", "CallStatusId", "dbo.CallStatus");
            DropForeignKey("dbo.CallLog", "Call_CallLogId", "dbo.CallLog");
            DropForeignKey("dbo.CallAppointment", "CallAppointmentId", "dbo.CallLog");
            DropForeignKey("dbo.LeadUserMappingHistory", "AspNetUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.LeadUserMappingHistory", "LeadId", "dbo.Lead");
            DropForeignKey("dbo.AddressUserMapping", "AddressId", "dbo.Address");
            DropIndex("dbo.LeadSourceCampaign", new[] { "Campaign_CampaignId" });
            DropIndex("dbo.LeadSourceCampaign", new[] { "LeadSource_LeadSourceId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.LeadLeadStatusMapping", new[] { "LeadStatusId" });
            DropIndex("dbo.LeadLeadStatusMapping", new[] { "LeadId" });
            DropIndex("dbo.County", new[] { "StateId" });
            DropIndex("dbo.LeadSourceSubCategory", new[] { "LeadSourceId" });
            DropIndex("dbo.DupliacteContractRequest", new[] { "LeadId" });
            DropIndex("dbo.CallAppointment", new[] { "CallAppointmentId" });
            DropIndex("dbo.CallLog", new[] { "Call_CallLogId" });
            DropIndex("dbo.CallLog", new[] { "CallStatusId" });
            DropIndex("dbo.CallLog", new[] { "LeadId" });
            DropIndex("dbo.LeadUserMappingHistory", new[] { "AspNetUserId" });
            DropIndex("dbo.LeadUserMappingHistory", new[] { "LeadId" });
            DropIndex("dbo.Lead", new[] { "LeadState_StateId" });
            DropIndex("dbo.Lead", new[] { "LeadCounty_CountyId" });
            DropIndex("dbo.Lead", new[] { "LeadSourceId" });
            DropIndex("dbo.Lead", new[] { "LeadRankingId" });
            DropIndex("dbo.Lead", new[] { "CampaignId" });
            DropIndex("dbo.Lead", new[] { "ChannelId" });
            DropIndex("dbo.Lead", new[] { "AspNetUsersId" });
            DropIndex("dbo.Lead", new[] { "BillingAddressId" });
            DropIndex("dbo.Lead", new[] { "ServiceAddressId" });
            DropIndex("dbo.AspNetUsers", new[] { "UserType_UserTypeId" });
            DropIndex("dbo.AspNetUsers", new[] { "UserTypeId" });
            DropIndex("dbo.AspNetUsers", new[] { "PartnerId" });
            DropIndex("dbo.AspNetUsers", new[] { "UserParentId" });
            DropIndex("dbo.AddressUserMapping", new[] { "VisitStatusId" });
            DropIndex("dbo.AddressUserMapping", new[] { "UserId" });
            DropIndex("dbo.AddressUserMapping", new[] { "AddressId" });
            DropTable("dbo.LeadSourceCampaign");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.Audit");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.VisitStatus");
            DropTable("dbo.UserType");
            DropTable("dbo.Partner");
            DropTable("dbo.LeadRanking");
            DropTable("dbo.LeadStatus");
            DropTable("dbo.LeadLeadStatusMapping");
            DropTable("dbo.State");
            DropTable("dbo.County");
            DropTable("dbo.Channel");
            DropTable("dbo.LeadSourceSubCategory");
            DropTable("dbo.LeadSource");
            DropTable("dbo.Campaign");
            DropTable("dbo.DupliacteContractRequest");
            DropTable("dbo.CallStatus");
            DropTable("dbo.CallAppointment");
            DropTable("dbo.CallLog");
            DropTable("dbo.LeadUserMappingHistory");
            DropTable("dbo.Lead");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AddressUserMapping");
            DropTable("dbo.AddressSource");
            DropTable("dbo.Address");
        }
    }
}
