namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DbSpacing : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Address", "AppartmentNo", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Address", "AddressInformation", c => c.String(maxLength: 400, unicode: false));
            AlterColumn("dbo.Address", "State", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Address", "City", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Address", "Zipcode", c => c.String(maxLength: 25, unicode: false));
            AlterColumn("dbo.Address", "County", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Lead", "PrimaryEmail", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Lead", "SecondaryEmail", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Lead", "FirstName", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("dbo.Lead", "LastName", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Lead", "DateOfBirth", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Lead", "UtilityAccountNo", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Lead", "UtilityCompanyName", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("dbo.Lead", "HomePhoneNo", c => c.String(maxLength: 25, unicode: false));
            AlterColumn("dbo.Lead", "WorkPhoneNo", c => c.String(maxLength: 25, unicode: false));
            AlterColumn("dbo.Lead", "MobileNo", c => c.String(maxLength: 25, unicode: false));
            AlterColumn("dbo.Lead", "RefferedBy", c => c.String(maxLength: 25, unicode: false));
            AlterColumn("dbo.Lead", "SSAType", c => c.String(maxLength: 25, unicode: false));
            AlterColumn("dbo.Lead", "EnvelopeId", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Lead", "ReasonToClose", c => c.String(maxLength: 250, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Lead", "ReasonToClose", c => c.String());
            AlterColumn("dbo.Lead", "EnvelopeId", c => c.String());
            AlterColumn("dbo.Lead", "SSAType", c => c.String());
            AlterColumn("dbo.Lead", "RefferedBy", c => c.String());
            AlterColumn("dbo.Lead", "MobileNo", c => c.String());
            AlterColumn("dbo.Lead", "WorkPhoneNo", c => c.String());
            AlterColumn("dbo.Lead", "HomePhoneNo", c => c.String());
            AlterColumn("dbo.Lead", "UtilityCompanyName", c => c.String());
            AlterColumn("dbo.Lead", "UtilityAccountNo", c => c.String());
            AlterColumn("dbo.Lead", "DateOfBirth", c => c.String());
            AlterColumn("dbo.Lead", "LastName", c => c.String());
            AlterColumn("dbo.Lead", "FirstName", c => c.String());
            AlterColumn("dbo.Lead", "SecondaryEmail", c => c.String());
            AlterColumn("dbo.Lead", "PrimaryEmail", c => c.String());
            AlterColumn("dbo.Address", "County", c => c.String());
            AlterColumn("dbo.Address", "Zipcode", c => c.String());
            AlterColumn("dbo.Address", "City", c => c.String());
            AlterColumn("dbo.Address", "State", c => c.String());
            AlterColumn("dbo.Address", "AddressInformation", c => c.String());
            AlterColumn("dbo.Address", "AppartmentNo", c => c.String());
        }
    }
}
