﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunshareDatabase
{
    public class AspNetUserRoles : BaseEntity
    {
        [Key,Column(Order =0)]
        [Required]
        public int UserId { get; set; }
        [Key,Column(Order =1)]
        [Required]
        public int RoleId { get; set; }

        [NotMapped]
        [ForeignKey("UserId")]
        public AspNetRoles UserRole { get; set; }
        [NotMapped]
        [ForeignKey("RoleId")]
        public AspNetUsers User { get; set; }
    }
}
