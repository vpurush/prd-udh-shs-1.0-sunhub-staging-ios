﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class Address : BaseEntity
    {
        public Address()
        {
        }
        public int AddressId { get; set; }
        public string AppartmentNo { get; set; }
        public string AddressInformation { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        public string County { get; set; }
        public float Lattitude { get; set; }
        public float Longitude { get; set; }
        public int AddressSourceId { get; set; }
        public bool IsActive { get; set; }
        public float DeviceLat { get; set; }
        public float DeviceLong { get; set; }
    }
}