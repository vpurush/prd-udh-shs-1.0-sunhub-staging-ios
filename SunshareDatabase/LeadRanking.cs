﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class LeadRanking : BaseEntity
    {
        public LeadRanking()
        {
        }
        public  virtual ICollection<Lead> Leads { get; set; }
        [Key]
        public int LeadRankingId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string RankingName { get; set; }
    }
}
