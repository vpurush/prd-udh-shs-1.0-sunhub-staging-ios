﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SunshareDatabase
{
    public class LeadUserMappingHistory : BaseEntity
    {
        public int LeadUserMappingHistoryId { get; set; }
        public int LeadId { get; set; }

        public int AspNetUserId { get; set; }

        [NotMapped]
        [ForeignKey("LeadId")]
        public virtual Lead mappedLead { get; set; }

        [NotMapped]
        [ForeignKey("AspNetUserId")]
        public virtual AspNetUsers mappedUser {get;set;}
    }
}
