﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class County : BaseEntity
    {
        public County()
        {
        }

        public int CountyId { get; set; }
        public string CountyName { get; set; }
        public bool IsActive { get; set; }
        public int StateId { get; set; }
        [NotMapped]
        [ForeignKey("StateId")]
        public virtual State CountyState { get; set; }
        [NotMapped]
        public virtual ICollection<Lead> Leads { get; set; }

    }
}
