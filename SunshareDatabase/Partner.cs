﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class Partner: BaseEntity
    {
        public Partner()
        {
        }
        [Key]
        public int PartnerId { get; set; }
        public string PartnerNumber { get; set; }
        public string PartnerName { get; set; }
        //public string PartnerDiscription { get; set; }
        public string PartnerLogoUrl { get; set; }
        public bool IsActive { get; set; }
        //public virtual ICollection<AspNetUsers> Users { get; set; }

        
    }
}