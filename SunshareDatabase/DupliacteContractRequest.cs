﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunshareDatabase
{
    public class DupliacteContractRequest
    {
        public DupliacteContractRequest()
        {
            
        }
        public int DupliacteContractRequestId { get; set; }
        public int LeadId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(250)]
        public string Reason { get; set; }
        public bool IsDecided { get; set; }
        public bool IsApproved { get; set; }
        [ForeignKey("LeadId")]
        public virtual Lead Lead { get; set; }
    }
}
