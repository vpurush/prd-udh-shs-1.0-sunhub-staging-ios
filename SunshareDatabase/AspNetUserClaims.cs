﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunshareDatabase
{
    public class AspNetUserClaims : BaseEntity
    {
        public int Id { get; set; }
        public int UserId { get; set; }

        public string ClaimType { get; set; }

        public string ClaimValue { get; set; }

        [NotMapped]
        public AspNetUsers User { get; set; }
    }
}
