﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class Campaign : BaseEntity
    {
        public Campaign()
        {
        }
        public int CampaignId { get; set; }
        public string CampaignName { get; set; }
        public string AmountSpent { get; set; }
        public string Description { get; set; }
        public string TypeOfCampaign { get; set; }
        public string Incentive { get; set; }
        public bool IsActive { get; set; }
        public int ChannelId { get; set; }
        public virtual ICollection<Lead> Leads { get; set; }
        public virtual Channel Channel { get; set; }


    }
}