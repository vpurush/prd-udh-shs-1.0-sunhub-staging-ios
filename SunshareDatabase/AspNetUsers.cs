﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunshareDatabase
{
    public class AspNetUsers : BaseEntity
    {
        public AspNetUsers()
        {
            IsActive = true;
        }
        [Key]
        public int Id { get; set; }

        public int? UserParentId { get; set; }

        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        public string PasswordHash { get; set; }

        public string SecurityStamp { get; set; }

        public long? PhoneNumber { get; set; }

        public bool PhoneNumberConfirmed { get; set; }

        public bool TwoFactorEnabled { get; set; }

        public DateTime? LockoutEndDateUtc { get; set; }

        public bool LockoutEnabled { get; set; }

        public int AccessFailedCount { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int PartnerId { get; set; }

        public int UserTypeId { get; set; }      
          
        public bool IsActive { get; set; }

        public string ImageUrl { get; set; }

        public string DeviceName { get; set; }

        public string DeviceSerialNo { get; set;}

        [NotMapped]
        public UserType UserType { get; set; }
        [NotMapped]
        public Partner Partner { get; set; }
        [NotMapped]
        public ICollection<AspNetUserRoles> UserRoles { get; set; }

        [NotMapped]
        [ForeignKey("UserParentId")]
        public AspNetUsers User { get; set; }

        [NotMapped]
        public ICollection<Lead> Leads { get; set; }

        [NotMapped]
        [ForeignKey("UserTypeId")]
        public virtual UserType Type { get; set; }
        [NotMapped]
        public virtual ICollection<LeadUserMappingHistory> AssignHistory { get; set; }

    }
}
